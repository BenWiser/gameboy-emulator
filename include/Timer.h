#ifndef _TIMER_H_
#define _TIMER_H_

#include <memory>
#include "Mmu.h"

class Timer
{
public:
    Timer(std::shared_ptr<Mmu> mmu);
    void tick(unsigned int cycle);

private:
    std::shared_ptr<Mmu> _mmu;
    unsigned int _divider_cycle;
};

#endif // _TIMER_H_