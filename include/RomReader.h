#ifndef _ROM_READER_H_
#define _ROM_READER_H_

#include <string>

#include "types/RomData.hpp"

namespace RomReader
{

rom_data read(std::string file_name);

} // namespace RomReader

#endif //_ROM_READER_H_