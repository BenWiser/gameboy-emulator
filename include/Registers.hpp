#ifndef _REGISTERS_H_
#define _REGISTERS_H_

#include "types/Byte.hpp"

struct Registers
{
    byte A;
    byte F;
    /**
     * The F register is the Program Status Word:
     * 7th bit - Zero Flag (Z) This is set if a math operation results in zero or two values match with CP
     * 6th bit - Subtraction Flag (N) This is set if a subtraction was performed in the last math op
     * 5th bit - Half Carry Flag (H) TODO: Add description
     * 4th bit - Carry Flag (C) TODO: Add description
     */

    byte B;
    byte C;

    byte D;
    byte E;

    byte H;
    byte L;

    byte S;
    byte P;
};

#endif // _REGISTERS_H_