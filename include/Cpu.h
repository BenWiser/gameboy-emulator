#ifndef _CPU_H_
#define _CPU_H_

#include <string>
#include <functional>
#include <memory>

#include "types/RomData.hpp"
#include "types/Byte.hpp"
#include "types/Address.h"
#include "InterruptManager.h"
#include "Registers.hpp"
#include "Mmu.h"

class Cpu
{
public:
    Cpu(std::shared_ptr<InterruptManager> interruptManager, std::shared_ptr<Mmu> mmu);
    bool tick(unsigned long& cycle);

    // Visible for testing
    Registers registers;

private:
    std::shared_ptr<InterruptManager> _interruptManager;
    std::shared_ptr<Mmu> _mmu;
    unsigned long _cycle;

    void _handleInterrupts();
    bool _executeCommand();
    bool _execute16bitCommand();

    void _increaseCycle(unsigned long amount = 1);

    bool _getZFlag();
    /**
     * Uses the first bit of the value to set the flag
     */
    void _setZFlag(bool value);

    bool _getCarryFlag();
    void _setCarryFlag(bool value);

    bool _getHalfCarryFlag();
    void _setHalfCarryFlag(bool value);

    bool _getSubtractFlag();
    void _setSubtractFlag(bool value);

    void _pushProgramCounterToStack();
    void _popStackToProgramCounter();

    void _restart(std::string name, unsigned int address);

    void _storeValueToAddress(std::string name, Address address, byte valueRegister);
    void _incrementRegister(std::string name, byte& reg);
    void _decrementRegister(std::string name, byte& reg);

    void _incrementRegisterPair(byte& register_low, byte& register_high);
    void _decrementRegisterPair(byte& register_low, byte& register_high);

    void _incrementRegisterPairInstruction(std::string name, byte& register_low, byte& register_high);
    void _decrementRegisterPairInstruction(std::string name, byte& register_low, byte& register_high);

    void _orRegister(std::string name, byte& reg);
    void _andRegister(std::string name, byte& reg);
    void _xor(std::string name, byte value);

    void _swap(std::string name, byte& reg);

    void _compare(std::string name, byte value);

    void _loadRegister(std::string name, byte& reg);
    void _loadValueToRegister(std::string name, byte& reg, byte value);
    void _loadRegisterPair(std::string name, byte& register_low, byte& register_high);

    void _addRegisters(std::string name, byte& register1, byte& register2);
    void _subtractRegisters(std::string name, byte& register1, byte& register2);

    void _addRegisterPairs(std::string name, byte& register_low1, byte& register_high1, byte& register_low2,
        byte& register_high2);

    void _call(std::string name, byte address_low, byte address_high);

    void _resetBit(std::string name, byte bit, byte& reg);
    void _resetBit(std::string name, byte bit, Address&& address);

    void _setBit(std::string name, byte bit, byte& reg);
    void _setBit(std::string name, byte bit, Address&& address);

    void _copyBitComplementToZFlag(std::string name, byte bit, byte b);
    void _copyBitComplementToZFlag(std::string name, byte bit, Address address);

    void _pushStack(byte value);
    byte _popStack();
    void _pushRegisterPairToStack(std::string name, byte& register_low, byte& register_high);
    void _popStackToRegisterPair(std::string name, byte& register_low, byte& register_high);

    void _rotateRegisterLeft(std::string name, byte& reg);
    void _rotateRegisterLeftCarry(std::string name, byte& reg);
    void _rotateRegisterRightCarry(std::string name, byte& reg);
    void _slideRegisterLeft(std::string name, byte& reg);
    void _srl(std::string name, byte& reg);
    void _daa();

    void _addAndCarry(std::string name, byte& reg, Address address);
    void _addAndCarry(std::string name, byte& reg1, byte& reg2);

    void _ret(std::string name, std::function<bool()> condition);
    void _jump(std::string name, std::function<bool()> condition, bool is_relative);

    // Some operations expect a byte by reference but we don't actually want to pass one so
    // just have a wasted byte lying around to pass in these cases
    byte _wasted_byte;
};

#endif // _CPU_H_