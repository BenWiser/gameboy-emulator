#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

namespace constants
{
const int GAMEBOY_WIDTH = 160;
const int GAMEBOY_HEIGHT = 144;

const int GAMEBOY_VIEWPORT_WIDTH = 160;
const int GAMEBOY_VIEWPORT_HEIGHT = 144;

const int WINDOW_WIDTH = GAMEBOY_WIDTH << 2;
const int WINDOW_HEIGHT = GAMEBOY_HEIGHT << 2;

const int CYCLES_IN_SECOND = 69905;

} // namespace constants

#endif // _CONSTANTS_H_