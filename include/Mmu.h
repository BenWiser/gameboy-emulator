#ifndef _MMU_H_
#define _MMU_H_

#include <vector>

#include "types/Byte.hpp"
#include "types/RomData.hpp"
#include "types/Address.h"

class Mmu
{
public:
    Mmu(rom_data&& biosRomData, rom_data&& romData);
    void set(Address address, byte value);
    void set(unsigned int location, byte value);
    byte get(Address address);
    byte get(unsigned int location);
    int size();

    /**
     * Read the next byte the program counter is pointing to
     */
    byte next();

    /**
     * Check if there is a valid next byte to read
     */
    bool hasNext();

    unsigned int getProgramCounter();

    void setProgramCounter(Address address);
    void setProgramCounter(unsigned int program_counter);

    byte getLy();
    void setLy(byte value);

    byte getScrollX();
    byte getScrollY();

    bool getTileDataSelect();

    bool getBackgroundTileDataSelect();

    bool getLcdEnabled();

    bool isWindowEnabled();

    bool getSpritesEnabled();

private:
    unsigned int _programCounter;
    std::vector<byte> _biosMemory;
    std::vector<byte> _memory;
    bool _isInBios;

    void _dmaTransfer(byte value);

    byte _getLCDC();
};

#endif // _MMU_H_