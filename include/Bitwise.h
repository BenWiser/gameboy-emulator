#ifndef _BITWISE_H_
#define _BITWISE_H_

#include "types/Byte.hpp"

namespace bitwise
{
void setBit(byte& value, byte bit, bool is_on);
bool getBit(byte& value, byte bit);
} // bitwise

#endif // _BITWISE_H_