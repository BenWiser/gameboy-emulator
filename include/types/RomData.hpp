#ifndef _ROM_DATA_H_
#define _ROM_DATA_H_

#include <vector>

#include "types/Byte.hpp"

typedef std::vector<byte> rom_data;

#endif // _ROM_DATA_H_