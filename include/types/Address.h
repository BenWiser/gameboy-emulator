#ifndef _ADDRESS_H_
#define _ADDRESS_H_

#include "types/Byte.hpp"

class Address
{
public:
    Address(unsigned int address);
    Address(byte byte_low, byte byte_high);

    unsigned int getLocation() const;
    Address operator+(const int& offset);
    Address operator-(const int& offset);
private:
    const unsigned int _address;
};

#endif // _ADDRESS_H_