#ifndef _LOG_H_
#define _LOG_H_

#include <iostream>

#define DEBUG_LOGS 1
#define ENABLED_LOG_LEVEL 2

#define LOG_DEBUG 0
#define LOG_INFO 1
#define LOG_WARN 2
#define LOG_ERROR 3

#if DEBUG_LOGS == 1
#define BLUE_COLOR_CODE "\u001b[34m"
#define GREEN_COLOR_CODE "\u001b[32m"
#define WHITE_COLOR_CODE "\u001b[37m"
#define YELLOW_COLOR_CODE "\u001b[33m"
#define RED_COLOR_CODE "\u001b[31m"

#define LOG(tag, level, msg) do { if (level >= ENABLED_LOG_LEVEL) \
    std::cout << \
    (level == LOG_DEBUG ? BLUE_COLOR_CODE \
    : level == LOG_INFO ? GREEN_COLOR_CODE \
    : level == LOG_WARN ? YELLOW_COLOR_CODE \
    : level == LOG_ERROR ? RED_COLOR_CODE \
    : WHITE_COLOR_CODE) \
    << "[" << tag << "] " << WHITE_COLOR_CODE << msg << std::endl; } while(false)

#else
#define LOG(tag, level, msg) do { ; } while(false)
#endif

#endif // _LOG_H_