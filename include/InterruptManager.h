#ifndef _INTERRUPT_MANAGER_H_
#define _INTERRUPT_MANAGER_H_

#include <memory>

#include "Mmu.h"

class InterruptManager
{
public:
    InterruptManager(std::shared_ptr<Mmu> mmu);

    bool interruptsEnabled;

    void setVBlank(bool vBlank);
    bool getVBlank();

    void setLCD(bool lcd);
    bool getLCD();

    void setTimer(bool timer);
    bool getTimer();

    void setSerial(bool serial);
    bool getSerial();
    
    void setJoypad(bool joypad);
    bool getJoypad();

private:
    std::shared_ptr<Mmu> _mmu;

    void _setIF(byte value);
    byte _getIF();
};

#endif //_INTERRUPT_MANAGER_H_
