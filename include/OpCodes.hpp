#ifndef _INSTRUCTIONS_H_
#define _INSTRUCTIONS_H_

typedef const unsigned char opcode;

// 8 bit opcodes
// Just increases the program counter
opcode NOP = 0x0;

// Load the next two bytes into the following register pairs
opcode LD_BC = 0x01;
opcode LD_DE = 0x11;
opcode LD_SP = 0x31;
opcode LD_HL = 0x21;

// Load the next byte into the following register
opcode LD_A = 0x3E;
opcode LD_B = 0x06;
opcode LD_C = 0x0E;
opcode LD_D = 0x16;
opcode LD_E = 0x1E;
opcode LD_H = 0x26;
opcode LD_L = 0x2E;

// Load the contents from one register to another
opcode LD_A_A = 0x7F;
opcode LD_A_B = 0x78;
opcode LD_A_C = 0x79;
opcode LD_A_D = 0x7A;
opcode LD_A_E = 0x7B;
opcode LD_A_H = 0x7C;
opcode LD_A_L = 0x7D;

opcode LD_B_A = 0x47;
opcode LD_B_B = 0x40;
opcode LD_B_C = 0x41;
opcode LD_B_D = 0x42;
opcode LD_B_E = 0x43;
opcode LD_B_H = 0x44;
opcode LD_B_L = 0x45;

opcode LD_C_A = 0x4F;
opcode LD_C_B = 0x48;
opcode LD_C_C = 0x49;
opcode LD_C_D = 0x4A;
opcode LD_C_E = 0x4B;
opcode LD_C_H = 0x4C;
opcode LD_C_L = 0x4D;

opcode LD_D_A = 0x57;
opcode LD_D_B = 0x50;
opcode LD_D_C = 0x51;
opcode LD_D_D = 0x52;
opcode LD_D_E = 0x53;
opcode LD_D_H = 0x54;
opcode LD_D_L = 0x55;

opcode LD_E_A = 0x5F;
opcode LD_E_B = 0x58;
opcode LD_E_C = 0x59;
opcode LD_E_D = 0x5A;
opcode LD_E_E = 0x5B;
opcode LD_E_H = 0x5C;
opcode LD_E_L = 0x5D;

opcode LD_H_A = 0x67;
opcode LD_H_B = 0x60;
opcode LD_H_C = 0x61;
opcode LD_H_D = 0x62;
opcode LD_H_E = 0x63;
opcode LD_H_H = 0x64;
opcode LD_H_L = 0x65;

opcode LD_L_A = 0x6F;
opcode LD_L_B = 0x68;
opcode LD_L_C = 0x69;
opcode LD_L_D = 0x6A;
opcode LD_L_E = 0x6B;
opcode LD_L_H = 0x6C;
opcode LD_L_L = 0x6D;

opcode LD_A_BC_LOC = 0x0A;

// Load the contents of the address pointed to by the next byte into the following register
opcode LD_A_LOC = 0xE0;
opcode LD_A_a8_LOC = 0xF0;
opcode LD_a16_LOC_A = 0xEA;
opcode LD_A_a16_LOC = 0xFA;
opcode LD_a16_LOC_SP = 0x08;

// Load the contents of A at the location specified by the contents of C + 0XFF00
opcode LD_C_LOC_A = 0xE2;
opcode LD_HL_LOC_A = 0x77;
opcode LD_HL_LOC_B = 0x70;
opcode LD_HL_LOC_C = 0x71;
opcode LD_HL_LOC_D = 0x72;
opcode LD_HL_LOC_E = 0x73;
opcode LD_HL_LOC_H = 0x74;
opcode LD_HL_LOC_L = 0x75;

opcode LD_HL_LOC_d8 = 0x36;

opcode LD_DE_LOC_A = 0x12;

opcode LD_A_DE_LOC = 0x1A;

opcode LD_A_HL_LOC = 0x7E;
opcode LD_B_HL_LOC = 0x46;
opcode LD_C_HL_LOC = 0x4E;
opcode LD_D_HL_LOC = 0x56;
opcode LD_E_HL_LOC = 0x5E;
opcode LD_H_HL_LOC = 0x66;
opcode LD_L_HL_LOC = 0x6E;

opcode LD_HL_MINUS_A = 0x32; // Store the contents of register A into the memory location that's set in the HL registers and also decrement the contents of HL
opcode LD_HL_PLUS_A = 0x22; // Store the contents of register A into the memory location that's set in the HL registers and also increment the contents of HL
opcode LD_A_HL_PLUS = 0x2A;
opcode LD_A_HL_MINUS = 0x3A;

opcode LD_BC_LOC_A = 0x02;

// Add the contents of one register to another
opcode ADD_A_A = 0x87;
opcode ADD_A_B = 0x80;
opcode ADD_A_C = 0x81;
opcode ADD_A_D = 0x82;
opcode ADD_A_E = 0x83;
opcode ADD_A_H = 0x84;
opcode ADD_A_L = 0x85;
opcode ADD_A_d8 = 0xC6;
opcode ADD_A_HL_LOC = 0x86;

opcode ADD_HL_BC = 0x09;
opcode ADD_HL_DE = 0x19;
opcode ADD_HL_HL = 0x29;
opcode ADD_HL_SP = 0x39;

opcode ADC_A_C = 0x89;
opcode ADC_A_HL_LOC = 0x8E;

// Subtract the contents of a register from register A and store in register A
opcode SUB_A = 0x97;
opcode SUB_B = 0x90;
opcode SUB_C = 0x91;
opcode SUB_D = 0x92;
opcode SUB_E = 0x93;
opcode SUB_H = 0x94;
opcode SUB_L = 0x95;
opcode SUB_d8 = 0xD6;
opcode SUB_HL_LOC = 0x96;

// Increment the contents of the register
opcode INC_A = 0x3C;
opcode INC_B = 0x04;
opcode INC_C = 0x0C;
opcode INC_D = 0x14;
opcode INC_E = 0x1C;
opcode INC_H = 0x24;
opcode INC_L = 0x2C;

// Increment the contents of the register pairs
opcode INC_BC = 0x03;
opcode INC_DE = 0x13;
opcode INC_HL = 0x23;
opcode INC_SP = 0x33;

// Decrement the contents of the register
opcode DEC_A = 0x3D;
opcode DEC_B = 0x05;
opcode DEC_C = 0x0D;
opcode DEC_D = 0x15;
opcode DEC_E = 0x1D;
opcode DEC_H = 0x25;
opcode DEC_L = 0x2D;

opcode DEC_BC = 0x0B;
opcode DEC_DE = 0x1B;
opcode DEC_HL = 0x2B;
opcode DEC_SP = 0x3B;

opcode INC_HL_LOC = 0x34;
opcode DEC_HL_LOC = 0x35;

// Logical operators
opcode OR_A = 0xB7;
opcode OR_B = 0xB0;
opcode OR_C = 0xB1;
opcode OR_D = 0xB2;
opcode OR_E = 0xB3;
opcode OR_H = 0xB4;
opcode OR_L = 0xB5;

opcode AND_A = 0xA7;
opcode AND_B = 0xA0;
opcode AND_C = 0xA1;
opcode AND_D = 0xA2;
opcode AND_E = 0xA3;
opcode AND_H = 0xA4;
opcode AND_L = 0xA5;

opcode XOR_A = 0xAF;
opcode XOR_B = 0xA8;
opcode XOR_C = 0xA9;
opcode XOR_D = 0xAA;
opcode XOR_E = 0xAB;
opcode XOR_H = 0xAC;
opcode XOR_L = 0xAD;
opcode XOR_HL_LOC = 0xAE;
opcode XOR_d8 = 0xEE;

opcode AND_d8 = 0xE6;
opcode OR_d8 = 0xF6;

// One's compliment of reg A
opcode CPL = 0x2f;

// Jump relative
opcode JR = 0x18; // Jump the provided steps from the next byte from the current program counter
opcode JR_Z = 0x28; // If the Z flag is set, jump the provided steps from the next byte from the current program counter
opcode JR_NZ = 0x20; // If the Z flag is not set, jump the provided steps from the next byte from the current program counter
opcode JR_C = 0x38;
opcode JR_NC = 0x30;

// Jump exact commands
opcode JP_HL = 0xE9;
opcode JP_a16 = 0xC3;
opcode JP_Z_a16 = 0xCA;
opcode JP_NZ_a16 = 0xC2;

// Stack opcodes
opcode CALL = 0xCD;
opcode CALL_NZ = 0xC4;
opcode RET_Z = 0xC8;
opcode RET_NZ = 0xC0;
opcode RET_C = 0xD8;
opcode RET_NC = 0xD0;
opcode RET = 0xC9;
opcode RETI = 0xD9;
opcode PUSH_BC = 0xC5;
opcode PUSH_DE = 0xD5;
opcode PUSH_HL = 0xE5;
opcode PUSH_AF = 0xF5;
opcode POP_BC = 0xC1;
opcode POP_DE = 0xD1;
opcode POP_HL = 0xE1;
opcode POP_AF = 0xF1;

// Flags
opcode SCF = 0x37;

// Comparison
opcode CP_A = 0xBF;
opcode CP_B = 0xB8;
opcode CP_C = 0xB9;
opcode CP_D = 0xBA;
opcode CP_E = 0xBB;
opcode CP_H = 0xBC;
opcode CP_L = 0xBD;
opcode CP_HL_LOC = 0xBE;
opcode CP_d8 = 0xFE;

// Disable interrupts
opcode DI = 0xF3;
opcode EI = 0xFB;

// Restarts
opcode RST_0 = 0xC7;
opcode RST_1 = 0xCF;
opcode RST_2 = 0xD7;
opcode RST_3 = 0xDF;
opcode RST_4 = 0xE7;
opcode RST_5 = 0xEF;
opcode RST_6 = 0xF7;
opcode RST_7 = 0xFF;

// Rotate contents of registers to the right
opcode RRCA = 0x0F;

opcode DAA = 0x27;

// 16 bit opcodes
opcode SIXTEEN_BIT_OPCODES = 0xCB; // All the 16 bit opcodes start with CB
// The 16 bit opcodes all start the same way so just track the second byte

// Rotate contents of registers to the left
opcode RLC_A = 0x07;
opcode RL_A = 0x17;
opcode RL_B = 0x10;
opcode RL_C = 0x11;
opcode RL_D = 0x12;
opcode RL_E = 0x13;
opcode RL_H = 0x14;
opcode RL_L = 0x15;

opcode SLA_A = 0x27;

// Swap the lower order nibble with the higher order nibble
opcode SWAP_A = 0x37;
opcode SWAP_B = 0x30;
opcode SWAP_C = 0x31;
opcode SWAP_D = 0x32;
opcode SWAP_E = 0x33;
opcode SWAP_H = 0x34;
opcode SWAP_L = 0x35;

// Reset a bit of a register
opcode RES_0_A = 0x87;
opcode RES_0_B = 0x80;
opcode RES_0_C = 0x81;
opcode RES_0_D = 0x82;
opcode RES_0_E = 0x83;
opcode RES_0_H = 0x84;
opcode RES_0_L = 0x85;
opcode RES_0_HL_LOC = 0x86;

opcode RES_1_A = 0x8F;
opcode RES_1_B = 0x88;
opcode RES_1_C = 0x89;
opcode RES_1_D = 0x8A;
opcode RES_1_E = 0x8B;
opcode RES_1_H = 0x8C;
opcode RES_1_L = 0x8D;
opcode RES_1_HL_LOC = 0x8E;

opcode RES_2_A = 0x97;
opcode RES_2_B = 0x90;
opcode RES_2_C = 0x91;
opcode RES_2_D = 0x92;
opcode RES_2_E = 0x93;
opcode RES_2_H = 0x94;
opcode RES_2_L = 0x95;
opcode RES_2_HL_LOC = 0x96;

opcode RES_3_A = 0x9F;
opcode RES_3_B = 0x98;
opcode RES_3_C = 0x99;
opcode RES_3_D = 0x9A;
opcode RES_3_E = 0x9B;
opcode RES_3_H = 0x9C;
opcode RES_3_L = 0x9D;
opcode RES_3_HL_LOC = 0x9E;

opcode RES_4_A = 0xA7;
opcode RES_4_B = 0xA0;
opcode RES_4_C = 0xA1;
opcode RES_4_D = 0xA2;
opcode RES_4_E = 0xA3;
opcode RES_4_H = 0xA4;
opcode RES_4_L = 0xA5;
opcode RES_4_HL_LOC = 0xA6;

opcode RES_5_A = 0xAF;
opcode RES_5_B = 0xA8;
opcode RES_5_C = 0xA9;
opcode RES_5_D = 0xAA;
opcode RES_5_E = 0xAB;
opcode RES_5_H = 0xAC;
opcode RES_5_L = 0xAD;
opcode RES_5_HL_LOC = 0xAE;

opcode RES_6_A = 0xB7;
opcode RES_6_B = 0xB0;
opcode RES_6_C = 0xB1;
opcode RES_6_D = 0xB2;
opcode RES_6_E = 0xB3;
opcode RES_6_H = 0xB4;
opcode RES_6_L = 0xB5;
opcode RES_6_HL_LOC = 0xB6;

opcode RES_7_A = 0xBF;
opcode RES_7_B = 0xB8;
opcode RES_7_C = 0xB9;
opcode RES_7_D = 0xBA;
opcode RES_7_E = 0xBB;
opcode RES_7_H = 0xBC;
opcode RES_7_L = 0xBD;
opcode RES_7_HL_LOC = 0xBE;

opcode BIT_0_A = 0x47;
opcode BIT_0_B = 0x40;
opcode BIT_0_C = 0x41;
opcode BIT_0_D = 0x42;
opcode BIT_0_E = 0x43;
opcode BIT_0_H = 0x44;
opcode BIT_0_L = 0x45;
opcode BIT_0_HL_LOC = 0x46;

opcode BIT_1_A = 0x4F;
opcode BIT_1_B = 0x48;
opcode BIT_1_C = 0x49;
opcode BIT_1_D = 0x4A;
opcode BIT_1_E = 0x4B;
opcode BIT_1_H = 0x4C;
opcode BIT_1_L = 0x4D;
opcode BIT_1_HL_LOC = 0x4E;

opcode BIT_2_A = 0x57;
opcode BIT_2_B = 0x50;
opcode BIT_2_C = 0x51;
opcode BIT_2_D = 0x52;
opcode BIT_2_E = 0x53;
opcode BIT_2_H = 0x54;
opcode BIT_2_L = 0x55;
opcode BIT_2_HL_LOC = 0x56;

opcode BIT_3_A = 0x5F;
opcode BIT_3_B = 0x58;
opcode BIT_3_C = 0x59;
opcode BIT_3_D = 0x5A;
opcode BIT_3_E = 0x5B;
opcode BIT_3_H = 0x5C;
opcode BIT_3_L = 0x5D;
opcode BIT_3_HL_LOC = 0x5E;

opcode BIT_4_A = 0x67;
opcode BIT_4_B = 0x60;
opcode BIT_4_C = 0x61;
opcode BIT_4_D = 0x62;
opcode BIT_4_E = 0x63;
opcode BIT_4_H = 0x64;
opcode BIT_4_L = 0x65;
opcode BIT_4_HL_LOC = 0x66;

opcode BIT_5_A = 0x6F;
opcode BIT_5_B = 0x68;
opcode BIT_5_C = 0x69;
opcode BIT_5_D = 0x6A;
opcode BIT_5_E = 0x6B;
opcode BIT_5_H = 0x6C;
opcode BIT_5_L = 0x6D;
opcode BIT_5_HL_LOC = 0x6E;

opcode BIT_6_A = 0x77;
opcode BIT_6_B = 0x70;
opcode BIT_6_C = 0x71;
opcode BIT_6_D = 0x72;
opcode BIT_6_E = 0x73;
opcode BIT_6_H = 0x74;
opcode BIT_6_L = 0x75;
opcode BIT_6_HL_LOC = 0x76;

opcode BIT_7_A = 0x7F;
opcode BIT_7_B = 0x78;
opcode BIT_7_C = 0x79;
opcode BIT_7_D = 0x7A;
opcode BIT_7_E = 0x7B;
opcode BIT_7_H = 0x7C;
opcode BIT_7_L = 0x7D;
opcode BIT_7_HL_LOC = 0x7E;

opcode SET_0_A = 0xC7;
opcode SET_0_B = 0xC0;
opcode SET_0_C = 0xC1;
opcode SET_0_D = 0xC2;
opcode SET_0_E = 0xC3;
opcode SET_0_H = 0xC4;
opcode SET_0_L = 0xC5;
opcode SET_0_HL_LOC = 0xC6;

opcode SET_1_A = 0xCF;
opcode SET_1_B = 0xC8;
opcode SET_1_C = 0xC9;
opcode SET_1_D = 0xCA;
opcode SET_1_E = 0xCB;
opcode SET_1_H = 0xCC;
opcode SET_1_L = 0xCD;
opcode SET_1_HL_LOC = 0xCE;

opcode SET_2_A = 0xD7;
opcode SET_2_B = 0xD0;
opcode SET_2_C = 0xD1;
opcode SET_2_D = 0xD2;
opcode SET_2_E = 0xD3;
opcode SET_2_H = 0xD4;
opcode SET_2_L = 0xD5;
opcode SET_2_HL_LOC = 0xD6;

opcode SET_3_A = 0xDF;
opcode SET_3_B = 0xD8;
opcode SET_3_C = 0xD9;
opcode SET_3_D = 0xDA;
opcode SET_3_E = 0xDB;
opcode SET_3_H = 0xDC;
opcode SET_3_L = 0xDD;
opcode SET_3_HL_LOC = 0xDE;

opcode SET_4_A = 0xE7;
opcode SET_4_B = 0xE0;
opcode SET_4_C = 0xE1;
opcode SET_4_D = 0xE2;
opcode SET_4_E = 0xE3;
opcode SET_4_H = 0xE4;
opcode SET_4_L = 0xE5;
opcode SET_4_HL_LOC = 0xE6;

opcode SET_5_A = 0xEF;
opcode SET_5_B = 0xE8;
opcode SET_5_C = 0xE9;
opcode SET_5_D = 0xEA;
opcode SET_5_E = 0xEB;
opcode SET_5_H = 0xEC;
opcode SET_5_L = 0xED;
opcode SET_5_HL_LOC = 0xEE;

opcode SET_6_A = 0xF7;
opcode SET_6_B = 0xF0;
opcode SET_6_C = 0xF1;
opcode SET_6_D = 0xF2;
opcode SET_6_E = 0xF3;
opcode SET_6_H = 0xF4;
opcode SET_6_L = 0xF5;
opcode SET_6_HL_LOC = 0xF6;

opcode SET_7_A = 0xFF;
opcode SET_7_B = 0xF8;
opcode SET_7_C = 0xF9;
opcode SET_7_D = 0xFA;
opcode SET_7_E = 0xFB;
opcode SET_7_H = 0xFC;
opcode SET_7_L = 0xFD;
opcode SET_7_HL_LOC = 0xFE;

opcode SRL_A = 0x3F;
opcode SRL_B = 0x38;
opcode SRL_C = 0x39;
opcode SRL_D = 0x3A;
opcode SRL_E = 0x3B;
opcode SRL_H = 0x3C;
opcode SRL_L = 0x3D;

#endif // _INSTRUCTIONS_H_