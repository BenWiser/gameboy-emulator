#ifndef _GPU_H_
#define _GPU_H_

#include <memory>
#include <vector>

#include "InterruptManager.h"
#include "Mmu.h"
#include "Constants.hpp"

class Gpu
{
public:
    Gpu(std::shared_ptr<InterruptManager> interruptManager, std::shared_ptr<Mmu> mmu);
    bool tick(unsigned long& cycle);
private:
    std::shared_ptr<InterruptManager> _interruptManager;
    std::shared_ptr<Mmu> _mmu;
    std::vector<byte> _pixels;
    // Track the last time a line was scanned
    unsigned long _lastLineCycle;
    unsigned int _mode;

    void _render();
    void _readSprites();
    void _readGraphicsMemory(byte line);
    void _readTileRow(int x, int y, byte byte1, byte byte2);
    void _loadColour(byte& colour, int position);
};

#endif // _GPU_H_