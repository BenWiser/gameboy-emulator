#ifndef _JOYPAD_H_
#define _JOYPAD_H_

#include <memory>

#include "InterruptManager.h"
#include "Mmu.h"

class Joypad
{
public:
    Joypad(std::shared_ptr<InterruptManager> interrupt_manager, std::shared_ptr<Mmu> mmu);

    void setRight(bool is_set);
    void setLeft(bool is_set);
    void setUp(bool is_set);
    void setDown(bool is_set);

    void setA(bool is_set);
    void setB(bool is_set);

    void setSelect(bool is_set);
    void setStart(bool is_set);

    void tick();
private:
    std::shared_ptr<InterruptManager> _interrupt_manager;
    std::shared_ptr<Mmu> _mmu;

    byte _direction_joyp;
    byte _button_joyp;

    void _resetJoypRegister();

    // Used to track if the button type changes
    bool _directionWasSelected;
};

#endif // _JOYPAD_H_