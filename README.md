# Bens Groovy Gameboy Emulator

This is a little holiday project to get Tetris on the gameboy running.

To set it up, you'll need glfw3 installed for rendering.

Then just run:
- `git submodule update --init`
- `cmake .`
- `make`
- `./game_boy_emulator`

Run tests by running:
- `make test`

This was written to run tetris so it expects `tetris.gb` in the assets directory.