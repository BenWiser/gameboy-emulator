#include "Timer.h"

#include "types/Address.h"
#include "Log.hpp"
#include "Constants.hpp"

Timer::Timer(std::shared_ptr<Mmu> mmu): _mmu(mmu)
{
}

void Timer::tick(unsigned int cycle)
{
    Address divider_address(0x04, 0xFF);
    _mmu->set(divider_address, cycle / ((constants::CYCLES_IN_SECOND / 16)));
}