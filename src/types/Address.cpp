#include "types/Address.h"

Address::Address(unsigned int address): _address(address)
{
}

Address::Address(byte byte_low, byte byte_high): _address((byte_high << 8) | byte_low)
{
}

unsigned int Address::getLocation() const
{
    return _address;
}

Address Address::operator+(const int& offset)
{
    return Address(_address + offset);
}

Address Address::operator-(const int& offset)
{
    return Address(_address - offset);
}