#include <GL/gl.h>

#include "Log.hpp"
#include "Constants.hpp"
#include "Gpu.h"
#include "Bitwise.h"
#include "types/Byte.hpp"

Gpu::Gpu(std::shared_ptr<InterruptManager> interruptManager, std::shared_ptr<Mmu> mmu): _interruptManager(interruptManager), _mmu(mmu), _pixels(constants::GAMEBOY_VIEWPORT_WIDTH * constants::GAMEBOY_VIEWPORT_HEIGHT * 3, 0),
    _lastLineCycle(0), _mode(0)
{
}

bool Gpu::tick(unsigned long& cycle)
{
    if (!_mmu->getLcdEnabled())
    {
        return false;
    }
    Address lcd_status_address(0x41, 0xFF);
    bool should_render = false;
    auto gpu_cycle_diff = cycle - _lastLineCycle;
    auto line = _mmu->getLy();
    auto line_changed = false;

    switch (_mode)
    {
        // OAM
        case 2:
        {
            if (gpu_cycle_diff >= 80)
            {
                _lastLineCycle = cycle;
                _mode = 3;
                _mmu->set(lcd_status_address, 0b11);
            }
            break;
        }
        // VRAM
        case 3:
        {
            if (gpu_cycle_diff >= 172)
            {
                _lastLineCycle = cycle;
                _mode = 0;
                _mmu->set(lcd_status_address, 0b00);
            }
            break;
        }
        // H BLANK
        case 0:
        {
            if (gpu_cycle_diff >= 204)
            {
                _lastLineCycle = cycle;
                line ++;
                line_changed = true;
                _readGraphicsMemory(line);

                if (line == 144)
                {
                    // Switch to VBlank
                    _mode = 1;
                    _mmu->set(lcd_status_address, 0b01);
                    _interruptManager->setVBlank(true);
                    should_render = true;
                }
                else
                {
                    // Switch to OAM
                    _mode = 2;
                    _mmu->set(lcd_status_address, 0b10);
                }
            }
            break;
        }
        // V BLANK
        case 1:
        {
            if (gpu_cycle_diff >= 456)
            {
                _lastLineCycle = cycle;
                line ++;
                line_changed = true;

                if (line > 154)
                {
                    // Switch to OAM
                    _mode = 2;
                    _mmu->set(lcd_status_address, 0b10);
                    line = 0;
                    _readSprites();
                    _render();
                }
            }
            break;
        }
    }

    if (line_changed)
    {
        _mmu->setLy(line);
    }

    return should_render;
}

void Gpu::_render()
{
    LOG("GPU", LOG_INFO, "Rendering pixels");

    glClear(GL_COLOR_BUFFER_BIT);

    glPixelZoom(constants::WINDOW_WIDTH / constants::GAMEBOY_VIEWPORT_WIDTH,
        constants::WINDOW_HEIGHT / constants::GAMEBOY_VIEWPORT_HEIGHT);

    glDrawPixels(constants::GAMEBOY_VIEWPORT_WIDTH, constants::GAMEBOY_VIEWPORT_HEIGHT,
        GL_RGB, GL_UNSIGNED_BYTE, _pixels.data());
}

void Gpu::_readSprites()
{
    if (!_mmu->getSpritesEnabled())
    {
        return;
    }

    LOG("GPU", LOG_DEBUG, "Render sprites");

    Address sprite_memory_address(0x00, 0xFE);

    for (unsigned int i = 0; i < 40; i ++)
    {
        // There are 4 byte per sprite attribute
        auto sprite_attribute_address = sprite_memory_address + (i * 4);

        auto y_position = _mmu->get(sprite_attribute_address) - 16;
        auto x_position = _mmu->get(sprite_attribute_address + 1) - 8;
        auto pattern_index = _mmu->get(sprite_attribute_address + 2);
        auto attributes = _mmu->get(sprite_attribute_address + 3);

        auto sprite_visible = bitwise::getBit(attributes, 7);
        auto y_flip = bitwise::getBit(attributes, 6);
        auto x_flip = bitwise::getBit(attributes, 5);
        auto pallette_index = bitwise::getBit(attributes, 4);

        if (x_position == 0 && y_position == 0)
        {
            return;
        }

        for (int y = 0; y < 8; y ++)
        {
            Address address(0x8000 + pattern_index * 16 + y * 2);
            byte byte1 = _mmu->get(address);
            byte byte2 = _mmu->get(address + 1);
            _readTileRow(x_position, y_position + y, byte1, byte2);
        }
    }
}

void Gpu::_readGraphicsMemory(byte line)
{
    if (line < 0 || line > constants::GAMEBOY_VIEWPORT_HEIGHT)
    {
        return;
    }

    // The gameboy rendered in scanlines so only read the current line the ly is pointing to
    LOG("GPU", LOG_DEBUG, "Loading line " << std::dec << (int) line);

    auto scroll_x = _mmu->getScrollX();
    auto scroll_y = _mmu->getScrollY();
    auto tile_data_select = _mmu->getTileDataSelect();
    auto background_data_select = _mmu->getBackgroundTileDataSelect();
    
    constexpr auto TILES_IN_A_LINE = 32;
    constexpr auto TILE_SIZE = 8;

    uint y = line + scroll_y;

    uint background_offset = background_data_select ? 0x9C00 : 0x9800;
    background_offset += (y / TILE_SIZE) * TILES_IN_A_LINE;

    for (uint x = 0; x < constants::GAMEBOY_VIEWPORT_WIDTH; x += TILE_SIZE)
    {
        uint background_address = background_offset + (x / TILE_SIZE);
        auto tile_index = _mmu->get(background_address);
        
        uint tile_location = tile_data_select ? 0x8000 : 0x8800;

        // Every tile is 16 bytes
        tile_location += (tile_index * 16);

        // The second tile set is signed so adjust for that
        if(!tile_data_select && tile_index < 128)
        {
            tile_location += 256;
        }

        // Every consecutive byte is a new line
        tile_location += (y % TILE_SIZE) * 2;

        auto tile_1 = _mmu->get(tile_location);
        auto tile_2 = _mmu->get(tile_location + 1);

        _readTileRow(x, line, tile_1, tile_2);
    }
}

void Gpu::_readTileRow(int x, int y, byte byte1, byte byte2)
{
    for (int offset_x = 0; offset_x < 8; offset_x++)
    {
        int position = ((x + offset_x) + ((constants::GAMEBOY_VIEWPORT_HEIGHT - y)
            * constants::GAMEBOY_VIEWPORT_WIDTH)) * 3;

        byte colour = (bitwise::getBit(byte1, 7 - offset_x) << 1)
            | bitwise::getBit(byte2, 7 - offset_x);

        _loadColour(colour, position);
    }
}

void Gpu::_loadColour(byte& colour, int position)
{
    if ((position < 0) || (position + 2) > _pixels.size())
    {
        return;
    }

    int renderColour = 0;
    switch (colour)
    {
        case 0b00: renderColour = 255; break; // White
        case 0b01: renderColour = 192; break; // Light grey
        case 0b10: renderColour = 96; break; // Dark grey
        case 0b11: renderColour = 0; break; // Black
        default: renderColour = 0; break; // Default to black if unknown
    }

    _pixels[position] = renderColour;
    _pixels[position + 1] = renderColour;
    _pixels[position + 2] = renderColour;
}
