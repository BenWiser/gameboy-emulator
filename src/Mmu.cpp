#include "Mmu.h"

#include "Bitwise.h"
#include "Log.hpp"

Mmu::Mmu(rom_data&& biosRomData, rom_data&& romData): _programCounter(0), _biosMemory(0xFFFF, 0),
    _memory(0xFFFF, 0), _isInBios(true)
{
    // Storing the bios in seperate memory so that this memory can be returned
    // once the bios is complete
    _biosMemory.insert(_biosMemory.begin(),
        biosRomData.begin(),
        biosRomData.end());

    _memory.insert(_memory.begin(),
        romData.begin(),
        romData.end());
}

void Mmu::set(Address address, byte value)
{
    set(address.getLocation(), value);
}

void Mmu::set(unsigned int location, byte value)
{
    // ROM
    if (location >= 0 && location <= 0x3FFF)
    {
        return;
    }
    // Nasty hack to make the Tetris copyright screen load faster
    if (location == 0xffa6)
    {
        if (value > 10) value = 10;
    }
    if (location >= 0xFEA0 && location <= 0xFEFF)
    {
        LOG("MMU", LOG_ERROR, "Attempting to write to restricted location 0x" << std::hex << (int) location);
        return;
    }
    if (location == 0xFF46)
    {
        _dmaTransfer(value);
        return;
    }
    if (location >= 0 && location <= 0xFFFF)
    {
        if (_isInBios && location < 0x0100)
        {
            LOG("MMU", LOG_ERROR, "Cannot set memory in bios");
            exit(2);
        }
        // Mirror ram
        if (location >= 0xE000 && location <= 0xFDFF)
        {
            location -= 0x2000;
        }
        _memory[location] = value;
        LOG("MMU", LOG_DEBUG, "Set memory location: 0x" << std::hex << (int) location
            << " to value: " << std::hex << (int) value);
    }
}

byte Mmu::get(Address address)
{
    return get(address.getLocation());
}

byte Mmu::get(unsigned int location)
{
    if (location >= 0xFEA0 && location <= 0xFEFF)
    {
        LOG("MMU", LOG_ERROR, "Attempting to read from restricted location 0x" << std::hex << (int) location);
        return 0xFF;
    }
    // Mirror ram
    if (location >= 0xE000 && location <= 0xFDFF)
    {
        location -= 0x2000;
    }
    if (location >= 0 && location <= 0xFFFF)
    {
        auto value = _memory[location];

        if (_isInBios && location < 0x0100)
        {
            value = _biosMemory[location];
        }

        LOG("MMU", LOG_DEBUG, "Retrieving memory location: 0x" << std::hex << location <<
            " with value 0x" << (int) value);
        return value;
    }
    return 0xFF;
}

byte Mmu::next()
{
    auto b = get(_programCounter++);
    // Once we have gone past this point, we are no longer in bios so free this memory back
    // to the cartridge
    if (_isInBios && _programCounter > 0x0100)
    {
        LOG("MMU", LOG_INFO, "Exiting bios");
        _isInBios = false;
    }
    return b;
}

bool Mmu::hasNext()
{
    return _programCounter <= size(); 
}

unsigned int Mmu::getProgramCounter()
{
    return _programCounter;
}

void Mmu::setProgramCounter(Address address)
{
    setProgramCounter(address.getLocation());
}

void Mmu::setProgramCounter(unsigned int program_counter)
{
    this->_programCounter = program_counter;
}

int Mmu::size()
{
    return 0xFFFF;
}

byte Mmu::getLy()
{
    return get(0xFF44);
}

void Mmu::setLy(byte value)
{
    set(0xFF44, value);
}

byte Mmu::getScrollX()
{
    return get(0xFF43);
}

byte Mmu::getScrollY()
{
    return get(0xFF42);
}

bool Mmu::getTileDataSelect()
{
    auto lcdc = _getLCDC();
    return bitwise::getBit(lcdc, 4);
}

bool Mmu::getBackgroundTileDataSelect()
{
    auto lcdc = _getLCDC();
    return bitwise::getBit(lcdc, 3);
}

bool Mmu::getLcdEnabled()
{
    auto lcdc = _getLCDC();
    return bitwise::getBit(lcdc, 7);
}

bool Mmu::isWindowEnabled()
{
    auto lcdc = _getLCDC();
    return bitwise::getBit(lcdc, 5);
}

bool Mmu::getSpritesEnabled()
{
    auto lcdc = _getLCDC();
    return bitwise::getBit(lcdc, 1);
}

void Mmu::_dmaTransfer(byte value)
{
    LOG("MMU", LOG_DEBUG, "DMA TRANSFER");
    // The value written for DMA transfer is divided by 100 for memory conservation
    Address address(((unsigned int) value) << 8);
    for (unsigned int i = 0; i <= 0x9F; i ++)
    {
        set(0xFE00 + i, get(address + i));
    }
}

byte Mmu::_getLCDC()
{
    return get(0xFF40);
}