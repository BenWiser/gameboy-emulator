#include "Joypad.h"
#include "Log.hpp"
#include "Bitwise.h"

Joypad::Joypad(std::shared_ptr<InterruptManager> interrupt_manager, std::shared_ptr<Mmu> mmu): _interrupt_manager(interrupt_manager), _mmu(mmu), _direction_joyp(0xFF), _button_joyp(0xFF)
{
}

void Joypad::setRight(bool is_set)
{
    bitwise::setBit(_direction_joyp, 0, !is_set);
}

void Joypad::setLeft(bool is_set)
{
    bitwise::setBit(_direction_joyp, 1, !is_set);
}

void Joypad::setUp(bool is_set)
{
    bitwise::setBit(_direction_joyp, 2, !is_set);
}

void Joypad::setDown(bool is_set)
{
    bitwise::setBit(_direction_joyp, 3, !is_set);
}

void Joypad::setA(bool is_set)
{
    bitwise::setBit(_button_joyp, 0, !is_set);
}

void Joypad::setB(bool is_set)
{
    bitwise::setBit(_button_joyp, 1, !is_set);
}

void Joypad::setSelect(bool is_set)
{
    bitwise::setBit(_button_joyp, 2, !is_set);
}

void Joypad::setStart(bool is_set)
{
    bitwise::setBit(_button_joyp, 3, !is_set);
}

void Joypad::tick()
{
    Address address(0x00, 0xFF);
    auto joyp = _mmu->get(address);
    auto new_joyp = joyp & 0xF0;

    auto is_direction_enabled = !bitwise::getBit(joyp, 4);
    auto is_button_enabled = !bitwise::getBit(joyp, 5);

    if (is_direction_enabled)
    {
        new_joyp = (joyp & 0xF0) | (_direction_joyp & 0x0F);
    }
    else if (is_button_enabled)
    {
        new_joyp = (joyp & 0xF0) | (_button_joyp & 0x0F);
    }

    if (joyp != new_joyp)
    {
        _mmu->set(address, new_joyp);
        if (_interrupt_manager->interruptsEnabled)
        {
            _interrupt_manager->setJoypad(true);
        }
    }
}
