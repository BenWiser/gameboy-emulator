#include "Bitwise.h"

namespace bitwise
{
void setBit(byte& value, byte bit, bool is_on)
{
    if (is_on)
    {
        value |= (1 << bit);
    }
    else
    {
        value &= ~(1 << bit);
    }
}

bool getBit(byte& value, byte bit)
{
    return (value >> bit) & 1 == 1;
}
} // bitwise
