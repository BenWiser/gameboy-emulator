#include "Cpu.h"

#include "Log.hpp"
#include "OpCodes.hpp"
#include "Bitwise.h"

Cpu::Cpu(std::shared_ptr<InterruptManager> interruptManager, std::shared_ptr<Mmu> mmu): _interruptManager(interruptManager),
    _mmu(mmu), _cycle(0)
{   
}

bool Cpu::tick(unsigned long& cycle)
{
    _cycle = cycle;
    _handleInterrupts();
    auto success = _executeCommand();
    cycle = _cycle;

    return success;
}

void Cpu::_handleInterrupts()
{
    if (!_interruptManager->interruptsEnabled)
    {
        return;
    }

    const Address interrupts_enabled_address(0xFFFF);
    auto interrupts_flag = _mmu->get(interrupts_enabled_address);

    if (!interrupts_flag)
    {
        return;
    }

    auto check_interrupt = [&](std::string name, bool interrupt_enabled, Address&& address, byte bit) {
        if (interrupt_enabled && bitwise::getBit(interrupts_flag, bit))
        {
            _pushProgramCounterToStack();
            _mmu->setProgramCounter(address);

            _interruptManager->interruptsEnabled = false;

            LOG("CPU", LOG_DEBUG, "Executing interrupt " << name);
            return true;
        }
        return false;
    };

    if (check_interrupt("VBLANK", _interruptManager->getVBlank(), Address(0x0040), 0))
    {
        _interruptManager->setVBlank(false);
        return;
    }

    if (check_interrupt("LCD", _interruptManager->getLCD(), Address(0x0048), 1))
    {
        _interruptManager->setLCD(false);
        return;
    }

    if (check_interrupt("TIMER", _interruptManager->getTimer(), Address(0x0050), 2))
    {
        _interruptManager->setTimer(false);
        return;
    }

    if (check_interrupt("SERIAL", _interruptManager->getSerial(), Address(0x0058), 3))
    {
        _interruptManager->setSerial(false);
        return;
    }

    if (check_interrupt("JOYPAD", _interruptManager->getJoypad(), Address(0x0060), 4))
    {
        _interruptManager->setJoypad(false);
        return;
    }
}

bool Cpu::_executeCommand()
{
    if (_mmu->hasNext())
    {
        auto instruction = _mmu->next();

        // TODO: Think of another way to store these commands so that this doesn't get so large
        switch (instruction)
        {
            case NOP: LOG("CPU", LOG_DEBUG, "NOP"); _increaseCycle(); break;
            case LD_A: _loadRegister("LD A", registers.A); break;
            case LD_B: _loadRegister("LD B", registers.B); break;
            case LD_C: _loadRegister("LD C", registers.C); break;
            case LD_D: _loadRegister("LD D", registers.D); break;
            case LD_E: _loadRegister("LD E", registers.E); break;
            case LD_H: _loadRegister("LD H", registers.H); break;
            case LD_L: _loadRegister("LD L", registers.L); break;

            case LD_BC: _loadRegisterPair("LD BC", registers.C, registers.B); break;
            case LD_DE: _loadRegisterPair("LD DE", registers.E, registers.D); break;
            case LD_HL: _loadRegisterPair("LD HL", registers.L, registers.H); break;
            case LD_SP: _loadRegisterPair("LD SP", registers.P, registers.S); break;

            case INC_A: _incrementRegister("INC A", registers.A); break;
            case INC_B: _incrementRegister("INC B", registers.B); break;
            case INC_C: _incrementRegister("INC C", registers.C); break;
            case INC_D: _incrementRegister("INC D", registers.D); break;
            case INC_E: _incrementRegister("INC E", registers.E); break;
            case INC_H: _incrementRegister("INC H", registers.H); break;
            case INC_L: _incrementRegister("INC L", registers.L); break;

            case INC_BC: _incrementRegisterPairInstruction("BC", registers.C, registers.B); break;
            case INC_DE: _incrementRegisterPairInstruction("DE", registers.E, registers.D); break;
            case INC_HL: _incrementRegisterPairInstruction("HL", registers.L, registers.H); break;
            case INC_SP: _incrementRegisterPairInstruction("SP", registers.P, registers.S); break;

            case DEC_A: _decrementRegister("DEC A", registers.A); break;
            case DEC_B: _decrementRegister("DEC B", registers.B); break;
            case DEC_C: _decrementRegister("DEC C", registers.C); break;
            case DEC_D: _decrementRegister("DEC D", registers.D); break;
            case DEC_E: _decrementRegister("DEC E", registers.E); break;
            case DEC_H: _decrementRegister("DEC H", registers.H); break;
            case DEC_L: _decrementRegister("DEC L", registers.L); break;

            case DEC_BC: _decrementRegisterPairInstruction("BC", registers.C, registers.B); break;
            case DEC_DE: _decrementRegisterPairInstruction("DE", registers.E, registers.D); break;
            case DEC_HL: _decrementRegisterPairInstruction("HL", registers.L, registers.H); break;
            case DEC_SP: _decrementRegisterPairInstruction("SP", registers.P, registers.S); break;

            case INC_HL_LOC:
            {
                Address address(registers.L, registers.H);
                auto value = _mmu->get(address) + 1;
                _mmu->set(address, value);

                _increaseCycle(3);
                _setZFlag((int) value == 0);
                _setSubtractFlag(false);
                _setHalfCarryFlag((value & 0x0F) == 0x00);

                LOG("CPU", LOG_DEBUG, "INC (HL)");
                break;
            }

            case DEC_HL_LOC:
            {
                Address address(registers.L, registers.H);
                auto value = _mmu->get(address) - 1;
                _mmu->set(address, value);

                _increaseCycle(3);
                _setZFlag((int) value == 0);
                _setSubtractFlag(true);
                _setHalfCarryFlag((value & 0x0F) == 0x0F);

                LOG("CPU", LOG_DEBUG, "DEC (HL)");
                break;
            }

            case LD_HL_LOC_d8: _increaseCycle(); _storeValueToAddress("LD (HL), d8", Address(registers.L, registers.H), _mmu->next()); break;

            case LD_BC_LOC_A: _storeValueToAddress("LD (BC), A", Address(registers.C, registers.B), registers.A); break;
            case LD_DE_LOC_A: _storeValueToAddress("LD (DE), A",  Address(registers.E, registers.D), registers.A); break;
            case LD_A_DE_LOC: _increaseCycle(); _loadValueToRegister("LD A, (DE)", registers.A, _mmu->get(Address(registers.E, registers.D))); break;
            
            case LD_A_HL_LOC: _increaseCycle(); _loadValueToRegister("LD A, (HL)", registers.A, _mmu->get(Address(registers.L, registers.H))); break;
            case LD_B_HL_LOC: _increaseCycle(); _loadValueToRegister("LD B, (HL)", registers.B, _mmu->get(Address(registers.L, registers.H))); break;
            case LD_C_HL_LOC: _increaseCycle(); _loadValueToRegister("LD C, (HL)", registers.C, _mmu->get(Address(registers.L, registers.H))); break;
            case LD_D_HL_LOC: _increaseCycle(); _loadValueToRegister("LD D, (HL)", registers.D, _mmu->get(Address(registers.L, registers.H))); break;
            case LD_E_HL_LOC: _increaseCycle(); _loadValueToRegister("LD E, (HL)", registers.E, _mmu->get(Address(registers.L, registers.H))); break;
            case LD_H_HL_LOC: _increaseCycle(); _loadValueToRegister("LD H, (HL)", registers.H, _mmu->get(Address(registers.L, registers.H))); break;
            case LD_L_HL_LOC: _increaseCycle(); _loadValueToRegister("LD L, (HL)", registers.L, _mmu->get(Address(registers.L, registers.H))); break;

            case LD_A_A: _loadValueToRegister("LD A, A", registers.A, registers.A); break;
            case LD_A_B: _loadValueToRegister("LD A, B", registers.A, registers.B); break;
            case LD_A_C: _loadValueToRegister("LD A, C", registers.A, registers.C); break;
            case LD_A_D: _loadValueToRegister("LD A, D", registers.A, registers.D); break;
            case LD_A_E: _loadValueToRegister("LD A, E", registers.A, registers.E); break;
            case LD_A_H: _loadValueToRegister("LD A, H", registers.A, registers.H); break;
            case LD_A_L: _loadValueToRegister("LD A, L", registers.A, registers.L); break;

            case LD_B_A: _loadValueToRegister("LD B, A", registers.B, registers.A); break;
            case LD_B_B: _loadValueToRegister("LD B, B", registers.B, registers.B); break;
            case LD_B_C: _loadValueToRegister("LD B, C", registers.B, registers.C); break;
            case LD_B_D: _loadValueToRegister("LD B, D", registers.B, registers.D); break;
            case LD_B_E: _loadValueToRegister("LD B, E", registers.B, registers.E); break;
            case LD_B_H: _loadValueToRegister("LD B, H", registers.B, registers.H); break;
            case LD_B_L: _loadValueToRegister("LD B, L", registers.B, registers.L); break;

            case LD_C_A: _loadValueToRegister("LD C, A", registers.C, registers.A); break;
            case LD_C_B: _loadValueToRegister("LD C, B", registers.C, registers.B); break;
            case LD_C_C: _loadValueToRegister("LD C, C", registers.C, registers.C); break;
            case LD_C_D: _loadValueToRegister("LD C, D", registers.C, registers.D); break;
            case LD_C_E: _loadValueToRegister("LD C, E", registers.C, registers.E); break;
            case LD_C_H: _loadValueToRegister("LD C, H", registers.C, registers.H); break;
            case LD_C_L: _loadValueToRegister("LD C, L", registers.C, registers.L); break;

            case LD_D_A: _loadValueToRegister("LD D, A", registers.D, registers.A); break;
            case LD_D_B: _loadValueToRegister("LD D, B", registers.D, registers.B); break;
            case LD_D_C: _loadValueToRegister("LD D, C", registers.D, registers.C); break;
            case LD_D_D: _loadValueToRegister("LD D, D", registers.D, registers.D); break;
            case LD_D_E: _loadValueToRegister("LD D, E", registers.D, registers.E); break;
            case LD_D_H: _loadValueToRegister("LD D, H", registers.D, registers.H); break;
            case LD_D_L: _loadValueToRegister("LD D, L", registers.D, registers.L); break;

            case LD_E_A: _loadValueToRegister("LD E, A", registers.E, registers.A); break;
            case LD_E_B: _loadValueToRegister("LD E, B", registers.E, registers.B); break;
            case LD_E_C: _loadValueToRegister("LD E, C", registers.E, registers.C); break;
            case LD_E_D: _loadValueToRegister("LD E, D", registers.E, registers.D); break;
            case LD_E_E: _loadValueToRegister("LD E, E", registers.E, registers.E); break;
            case LD_E_H: _loadValueToRegister("LD E, H", registers.E, registers.H); break;
            case LD_E_L: _loadValueToRegister("LD E, L", registers.E, registers.L); break;

            case LD_H_A: _loadValueToRegister("LD H, A", registers.H, registers.A); break;
            case LD_H_B: _loadValueToRegister("LD H, B", registers.H, registers.B); break;
            case LD_H_C: _loadValueToRegister("LD H, C", registers.H, registers.C); break;
            case LD_H_D: _loadValueToRegister("LD H, D", registers.H, registers.D); break;
            case LD_H_E: _loadValueToRegister("LD H, E", registers.H, registers.E); break;
            case LD_H_H: _loadValueToRegister("LD H, H", registers.H, registers.H); break;
            case LD_H_L: _loadValueToRegister("LD H, L", registers.H, registers.L); break;

            case LD_L_A: _loadValueToRegister("LD L, A", registers.L, registers.A); break;
            case LD_L_B: _loadValueToRegister("LD L, B", registers.L, registers.B); break;
            case LD_L_C: _loadValueToRegister("LD L, C", registers.L, registers.C); break;
            case LD_L_D: _loadValueToRegister("LD L, D", registers.L, registers.D); break;
            case LD_L_E: _loadValueToRegister("LD L, E", registers.L, registers.E); break;
            case LD_L_H: _loadValueToRegister("LD L, H", registers.L, registers.H); break;
            case LD_L_L: _loadValueToRegister("LD L, L", registers.L, registers.L); break;

            case DI: LOG("CPU", LOG_DEBUG, "DI"); _interruptManager->interruptsEnabled = false; _increaseCycle(); break;
            case EI: LOG("CPU", LOG_DEBUG, "EI"); _interruptManager->interruptsEnabled = true; _increaseCycle(); break;

            case LD_HL_MINUS_A:
            {
                // Set the location to the value in the A register
                _mmu->set(Address(registers.L, registers.H), registers.A);
                
                // And then decrement the registers
                _decrementRegisterPair(registers.L, registers.H);

                _increaseCycle(2);

                LOG("CPU", LOG_DEBUG, "LD (HL-), A");
                break;
            }
            case LD_HL_PLUS_A:
            {
                // Set the location to the value in the A register
                _mmu->set(Address(registers.L, registers.H), registers.A);

                // And then decrement the registers
                _incrementRegisterPair(registers.L, registers.H);

                _increaseCycle(2);

                LOG("CPU", LOG_DEBUG, "LD (HL+), A");
                break;
            }
            
            case LD_A_HL_MINUS:
            {
                registers.A = _mmu->get(Address(registers.L, registers.H));
                _decrementRegisterPair(registers.L, registers.H);
                _increaseCycle(2);
                LOG("CPU", LOG_DEBUG, "LD A, (HL-)");
                break;
            }

            case LD_A_HL_PLUS:
            {
                registers.A = _mmu->get(Address(registers.L, registers.H));
                _incrementRegisterPair(registers.L, registers.H);
                _increaseCycle(2);
                LOG("CPU", LOG_DEBUG, "LD A, (HL+)");
                break;
            }

            case OR_A: _orRegister("A", registers.A); break;
            case OR_B: _orRegister("B", registers.B); break;
            case OR_C: _orRegister("C", registers.C); break;
            case OR_D: _orRegister("D", registers.D); break;
            case OR_E: _orRegister("E", registers.E); break;
            case OR_H: _orRegister("H", registers.H); break;
            case OR_L: _orRegister("L", registers.L); break;
            case OR_d8:
            {
                registers.A |= _mmu->next();
                _increaseCycle(2);
                _setZFlag((int) registers.A == 0);
                _setCarryFlag(false);
                _setSubtractFlag(false);
                _setHalfCarryFlag(false);
                LOG("CPU", LOG_DEBUG, "OR d8");
                break;
            }

            case AND_A: _andRegister("A", registers.A); break;
            case AND_B: _andRegister("B", registers.B); break;
            case AND_C: _andRegister("C", registers.C); break;
            case AND_D: _andRegister("D", registers.D); break;
            case AND_E: _andRegister("E", registers.E); break;
            case AND_H: _andRegister("H", registers.H); break;
            case AND_L: _andRegister("L", registers.L); break;
            case AND_d8:
            {
                registers.A &= _mmu->next();
                _increaseCycle(2);
                _setZFlag((int) registers.A == 0);
                _setCarryFlag(false);
                _setSubtractFlag(false);
                _setHalfCarryFlag(false);

                LOG("CPU", LOG_DEBUG, "AND d8");
                break;
            }

            case XOR_A: _xor("A", registers.A); break;
            case XOR_B: _xor("B", registers.B); break;
            case XOR_C: _xor("C", registers.C); break;
            case XOR_D: _xor("D", registers.D); break;
            case XOR_E: _xor("E", registers.E); break;
            case XOR_H: _xor("H", registers.H); break;
            case XOR_L: _xor("L", registers.L); break;
            case XOR_HL_LOC:
            {
                auto pair = _mmu->get(Address(registers.L, registers.H));
                _increaseCycle();
                _xor(" (HL)", pair);
                break;
            }
            case XOR_d8:
            {
                _increaseCycle();
                auto d8 = _mmu->next();
                _xor("d8", d8);
                break;
            }

            case CPL:
            {
                registers.A = ~registers.A;
                _increaseCycle(1);
                _setSubtractFlag(true);
                LOG("CPU", LOG_DEBUG, "CPL");
                break;
            }

            case RL_A: _rotateRegisterLeft("A", registers.A); _setZFlag(false); break;
            case RLC_A: _rotateRegisterLeftCarry("A", registers.A); _increaseCycle(); _setZFlag(false); break;
            case RRCA: _rotateRegisterRightCarry("A", registers.A); _setZFlag(false); break;

            case LD_C_LOC_A: _storeValueToAddress("LD (0XFF00+C), A", Address(0xFF00 + registers.C), registers.A); break;

            case LD_HL_LOC_A: _storeValueToAddress("LD (HL), A",  Address(registers.L, registers.H), registers.A); break;
            case LD_HL_LOC_B: _storeValueToAddress("LD (HL), B",  Address(registers.L, registers.H), registers.B); break;
            case LD_HL_LOC_C: _storeValueToAddress("LD (HL), C",  Address(registers.L, registers.H), registers.C); break;
            case LD_HL_LOC_D: _storeValueToAddress("LD (HL), D",  Address(registers.L, registers.H), registers.D); break;
            case LD_HL_LOC_E: _storeValueToAddress("LD (HL), E",  Address(registers.L, registers.H), registers.E); break;
            case LD_HL_LOC_H: _storeValueToAddress("LD (HL), H",  Address(registers.L, registers.H), registers.H); break;
            case LD_HL_LOC_L: _storeValueToAddress("LD (HL), L",  Address(registers.L, registers.H), registers.L); break;

            case ADD_A_A: _addRegisters("A, A", registers.A, registers.A); break;
            case ADD_A_B: _addRegisters("A, B", registers.A, registers.B); break;
            case ADD_A_C: _addRegisters("A, C", registers.A, registers.C); break;
            case ADD_A_D: _addRegisters("A, D", registers.A, registers.D); break;
            case ADD_A_E: _addRegisters("A, E", registers.A, registers.E); break;
            case ADD_A_H: _addRegisters("A, H", registers.A, registers.H); break;
            case ADD_A_L: _addRegisters("A, L", registers.A, registers.L); break;
            case ADD_A_d8:
            {
                auto d8 = _mmu->next();
                _increaseCycle();
                _addRegisters("A, d8", registers.A, d8);
                break;
            }
            case ADD_A_HL_LOC:
            {
                auto pair = _mmu->get(Address(registers.L, registers.H));
                _increaseCycle();
                _addRegisters("A, (HL)", registers.A, pair);
                break;
            }


            case ADD_HL_BC: _increaseCycle(2); _addRegisterPairs("HL, BC", registers.L, registers.H, registers.C, registers.B); break;
            case ADD_HL_HL: _increaseCycle(2); _addRegisterPairs("HL, HL", registers.L, registers.H, registers.L, registers.H); break;
            case ADD_HL_DE: _increaseCycle(2); _addRegisterPairs("HL, DE", registers.L, registers.H, registers.E, registers.D); break;
            case ADD_HL_SP: _increaseCycle(2); _addRegisterPairs("HL, SP", registers.L, registers.H, registers.P, registers.S); break;

            case ADC_A_C: _addAndCarry("A, C", registers.A, registers.C); break;
            case ADC_A_HL_LOC: _addAndCarry("A, (HL)", registers.A, Address(registers.L, registers.H)); break;

            case SUB_A: _subtractRegisters("A", registers.A, registers.A); break;
            case SUB_B: _subtractRegisters("B", registers.A, registers.B); break;
            case SUB_C: _subtractRegisters("C", registers.A, registers.C); break;
            case SUB_D: _subtractRegisters("D", registers.A, registers.D); break;
            case SUB_E: _subtractRegisters("E", registers.A, registers.E); break;
            case SUB_H: _subtractRegisters("H", registers.A, registers.H); break;
            case SUB_L: _subtractRegisters("L", registers.A, registers.L); break;
            case SUB_d8:
            {
                auto d8 = _mmu->next();
                _increaseCycle();
                _subtractRegisters("A, d8", registers.A, d8);
                break;
            }
            case SUB_HL_LOC:
            {
                auto pair = _mmu->get(Address(registers.L, registers.H));
                _increaseCycle();
                _subtractRegisters("(HL)", registers.A, pair);
                break;
            }

            case RST_0: _restart("0", 0x00); break;
            case RST_1: _restart("1", 0x08); break;
            case RST_2: _restart("2", 0x10); break;
            case RST_3: _restart("3", 0x18); break;
            case RST_4: _restart("4", 0x20); break;
            case RST_5: _restart("5", 0x28); break;
            case RST_6: _restart("6", 0x30); break;
            case RST_7: _restart("7", 0x38); break;

            case LD_A_LOC:
            {
                auto val = _mmu->next();
                _mmu->set(0xFF00 + val, registers.A);
                _increaseCycle(3);
                LOG("CPU", LOG_DEBUG, "LD (0xFF00 + 0x" << std::hex << (int) val << "), A");
                break;
            }

            case LD_A_BC_LOC:
            {
                registers.A = _mmu->get(Address(registers.C, registers.B));
                _increaseCycle(2);
                LOG("CPU", LOG_DEBUG, "LD A, (BC)");
                break;
            }

            case LD_A_a8_LOC:
            {
                auto val = _mmu->next();
                registers.A = _mmu->get(Address(0XFF00 + val));
                _increaseCycle(3);
                LOG("CPU", LOG_DEBUG, "LD A, (0xFF00 + 0x" << std::hex << (int) val << ")");
                break;
            }

            case LD_a16_LOC_A:
            {
                auto low = _mmu->next();
                auto high = _mmu->next();

                _mmu->set(Address(low, high), registers.A);

                _increaseCycle(4);
                LOG("CPU", LOG_DEBUG, "LD (a16), A");
                break;
            }

            case LD_A_a16_LOC:
            {
                auto low = _mmu->next();
                auto high = _mmu->next();

                registers.A = _mmu->get(Address(low, high));

                _increaseCycle(4);
                LOG("CPU", LOG_DEBUG, "LD A, (a16)");
                break;
            }

            case LD_a16_LOC_SP:
            {
                auto low = _mmu->next();
                auto high = _mmu->next();
                Address address(low, high);

                _mmu->set(address, registers.P);
                _mmu->set(address + 1, registers.S);

                _increaseCycle(5);
                LOG("CPU", LOG_DEBUG, "LD (a16), SP");
                break;
            }

            case JR: _jump("s8", [&] () { return true; }, true); break;
            case JR_Z: _jump("Z", [&] () { return _getZFlag(); }, true); break;
            case JR_NZ: _jump("NZ", [&] () { return !_getZFlag(); }, true); break;
            case JR_C: _jump("C", [&] () { return _getCarryFlag(); }, true); break;
            case JR_NC: _jump("NC", [&] () { return !_getCarryFlag(); }, true); break;

            case JP_HL: {
                _mmu->setProgramCounter(Address(registers.L, registers.H));
                _increaseCycle();
                LOG("CPU", LOG_DEBUG, "JP HL");
                break;
            }

            case JP_a16: _jump("a16", [&] () { return true; }, false); break;
            case JP_Z_a16: _jump("Z", [&] () { return _getZFlag(); }, false); break;
            case JP_NZ_a16: _jump("NZ", [&] () { return !_getZFlag(); }, false); break;

            case CALL:
            {
                auto low = _mmu->next();
                auto high = _mmu->next();
                _call("CALL", low, high);
                break;
            }

            case CALL_NZ:
            {
                auto low = _mmu->next();
                auto high = _mmu->next();
                if (!_getZFlag()) _call("CALL", low, high);
                break;
            }

            case RET_Z: _ret("Z", [&]() { return _getZFlag(); }); break;
            case RET_NZ: _ret("NZ", [&]() { return !_getZFlag(); }); break;
            case RET_C: _ret("C", [&]() { return _getCarryFlag(); }); break;
            case RET_NC: _ret("NC", [&]() { return !_getCarryFlag(); }); break;
            case RET: _ret("", []() { return true; }); break;
            case RETI: _ret("", []() { return true; }); _interruptManager->interruptsEnabled = true; break;

            case PUSH_BC: _pushRegisterPairToStack("BC", registers.C, registers.B); break;
            case PUSH_DE: _pushRegisterPairToStack("DE", registers.E, registers.D); break;
            case PUSH_HL: _pushRegisterPairToStack("HL", registers.L, registers.H); break;
            case PUSH_AF: _pushRegisterPairToStack("AF", registers.F, registers.A); break;

            case POP_BC: _popStackToRegisterPair("BC", registers.C, registers.B); break;
            case POP_DE: _popStackToRegisterPair("DE", registers.E, registers.D); break;
            case POP_HL: _popStackToRegisterPair("HL", registers.L, registers.H); break;
            case POP_AF: _popStackToRegisterPair("AF", _wasted_byte, registers.A); break;

            case SCF: _setCarryFlag(true); _setSubtractFlag(false); _increaseCycle(); LOG("CPU", LOG_DEBUG, "SCF"); break;

            case CP_A: _increaseCycle(-1); _compare("A", registers.A); break;
            case CP_B: _increaseCycle(-1); _compare("B", registers.B); break;
            case CP_C: _increaseCycle(-1); _compare("C", registers.C); break;
            case CP_D: _increaseCycle(-1); _compare("D", registers.D); break;
            case CP_E: _increaseCycle(-1); _compare("E", registers.E); break;
            case CP_H: _increaseCycle(-1); _compare("H", registers.H); break;
            case CP_L: _increaseCycle(-1); _compare("L", registers.L); break;
            case CP_HL_LOC: _compare("(HL)", _mmu->get(Address(registers.L, registers.H))); break;
            case CP_d8: _compare("d8", _mmu->next()); break;

            case DAA: _daa(); break;

            case SIXTEEN_BIT_OPCODES:
            {
                return _execute16bitCommand();
            }
            default:
            {
                LOG("CPU", LOG_ERROR, "Unrecognised op code 0x" << std::hex << (int) instruction << " at PG: 0x"
                    << std::hex << (int) _mmu->getProgramCounter());
                // Returning false indicates that the op code wasn't recognised
                return false;
            }
        }

        return true;
    }

    LOG("CPU", LOG_ERROR, "Tried to access program counter out of bounds ");

    return false;
}

bool Cpu::_execute16bitCommand()
{
    auto instruction = _mmu->next();
    switch (instruction)
    {
        case RL_A: _rotateRegisterLeft("A", registers.A); break;
        case RL_B: _rotateRegisterLeft("B", registers.B); break;
        case RL_C: _rotateRegisterLeft("C", registers.C); break;
        case RL_D: _rotateRegisterLeft("D", registers.D); break;
        case RL_E: _rotateRegisterLeft("E", registers.E); break;
        case RL_H: _rotateRegisterLeft("H", registers.H); break;
        case RL_L: _rotateRegisterLeft("L", registers.L); break;

        case SLA_A: _slideRegisterLeft("A A", registers.A); break;

        case SWAP_A: _swap("A", registers.A); break;
        case SWAP_B: _swap("B", registers.B); break;
        case SWAP_C: _swap("C", registers.C); break;
        case SWAP_D: _swap("D", registers.D); break;
        case SWAP_E: _swap("E", registers.E); break;
        case SWAP_H: _swap("H", registers.H); break;
        case SWAP_L: _swap("L", registers.L); break;

        case RES_0_A: _resetBit("0, A", 0, registers.A); break;
        case RES_0_B: _resetBit("0, B", 0, registers.B); break;
        case RES_0_C: _resetBit("0, C", 0, registers.C); break;
        case RES_0_D: _resetBit("0, D", 0, registers.D); break;
        case RES_0_E: _resetBit("0, E", 0, registers.E); break;
        case RES_0_H: _resetBit("0, H", 0, registers.H); break;
        case RES_0_L: _resetBit("0, L", 0, registers.L); break;
        case RES_0_HL_LOC: _resetBit("0, (HL)", 0, Address(registers.L, registers.H)); break;

        case RES_1_A: _resetBit("1, A", 1, registers.A); break;
        case RES_1_B: _resetBit("1, B", 1, registers.B); break;
        case RES_1_C: _resetBit("1, C", 1, registers.C); break;
        case RES_1_D: _resetBit("1, D", 1, registers.D); break;
        case RES_1_E: _resetBit("1, E", 1, registers.E); break;
        case RES_1_H: _resetBit("1, H", 1, registers.H); break;
        case RES_1_L: _resetBit("1, L", 1, registers.L); break;
        case RES_1_HL_LOC: _resetBit("1, (HL)", 1, Address(registers.L, registers.H)); break;

        case RES_2_A: _resetBit("2, A", 2, registers.A); break;
        case RES_2_B: _resetBit("2, B", 2, registers.B); break;
        case RES_2_C: _resetBit("2, C", 2, registers.C); break;
        case RES_2_D: _resetBit("2, D", 2, registers.D); break;
        case RES_2_E: _resetBit("2, E", 2, registers.E); break;
        case RES_2_H: _resetBit("2, H", 2, registers.H); break;
        case RES_2_L: _resetBit("2, L", 2, registers.L); break;
        case RES_2_HL_LOC: _resetBit("2, (HL)", 2, Address(registers.L, registers.H)); break;

        case RES_3_A: _resetBit("3, A", 3, registers.A); break;
        case RES_3_B: _resetBit("3, B", 3, registers.B); break;
        case RES_3_C: _resetBit("3, C", 3, registers.C); break;
        case RES_3_D: _resetBit("3, D", 3, registers.D); break;
        case RES_3_E: _resetBit("3, E", 3, registers.E); break;
        case RES_3_H: _resetBit("3, H", 3, registers.H); break;
        case RES_3_L: _resetBit("3, L", 3, registers.L); break;
        case RES_3_HL_LOC: _resetBit("0, (HL)", 3, Address(registers.L, registers.H)); break;

        case RES_4_A: _resetBit("4, A", 4, registers.A); break;
        case RES_4_B: _resetBit("4, B", 4, registers.B); break;
        case RES_4_C: _resetBit("4, C", 4, registers.C); break;
        case RES_4_D: _resetBit("4, D", 4, registers.D); break;
        case RES_4_E: _resetBit("4, E", 4, registers.E); break;
        case RES_4_H: _resetBit("4, H", 4, registers.H); break;
        case RES_4_L: _resetBit("4, L", 4, registers.L); break;
        case RES_4_HL_LOC: _resetBit("4, (HL)", 4, Address(registers.L, registers.H)); break;

        case RES_5_A: _resetBit("5, A", 5, registers.A); break;
        case RES_5_B: _resetBit("5, B", 5, registers.B); break;
        case RES_5_C: _resetBit("5, C", 5, registers.C); break;
        case RES_5_D: _resetBit("5, D", 5, registers.D); break;
        case RES_5_E: _resetBit("5, E", 5, registers.E); break;
        case RES_5_H: _resetBit("5, H", 5, registers.H); break;
        case RES_5_L: _resetBit("5, L", 5, registers.L); break;
        case RES_5_HL_LOC: _resetBit("5, (HL)", 5, Address(registers.L, registers.H)); break;

        case RES_6_A: _resetBit("6, A", 6, registers.A); break;
        case RES_6_B: _resetBit("6, B", 6, registers.B); break;
        case RES_6_C: _resetBit("6, C", 6, registers.C); break;
        case RES_6_D: _resetBit("6, D", 6, registers.D); break;
        case RES_6_E: _resetBit("6, E", 6, registers.E); break;
        case RES_6_H: _resetBit("6, H", 6, registers.H); break;
        case RES_6_L: _resetBit("6, L", 6, registers.L); break;
        case RES_6_HL_LOC: _resetBit("6, (HL)", 6, Address(registers.L, registers.H)); break;

        case RES_7_A: _resetBit("7, A", 7, registers.A); break;
        case RES_7_B: _resetBit("7, B", 7, registers.B); break;
        case RES_7_C: _resetBit("7, C", 7, registers.C); break;
        case RES_7_D: _resetBit("7, D", 7, registers.D); break;
        case RES_7_E: _resetBit("7, E", 7, registers.E); break;
        case RES_7_H: _resetBit("7, H", 7, registers.H); break;
        case RES_7_L: _resetBit("7, L", 7, registers.L); break;
        case RES_7_HL_LOC: _resetBit("7, (HL)", 7, Address(registers.L, registers.H)); break;

        case BIT_0_A: _copyBitComplementToZFlag("A", 0, registers.A); break;
        case BIT_0_B: _copyBitComplementToZFlag("B", 0, registers.B); break;
        case BIT_0_C: _copyBitComplementToZFlag("C", 0, registers.C); break;
        case BIT_0_D: _copyBitComplementToZFlag("D", 0, registers.D); break;
        case BIT_0_E: _copyBitComplementToZFlag("E", 0, registers.E); break;
        case BIT_0_H: _copyBitComplementToZFlag("H", 0, registers.H); break;
        case BIT_0_L: _copyBitComplementToZFlag("L", 0, registers.L); break;
        case BIT_0_HL_LOC: _copyBitComplementToZFlag("HL", 0, Address(registers.L, registers.H)); break;

        case BIT_1_A: _copyBitComplementToZFlag("A", 1, registers.A); break;
        case BIT_1_B: _copyBitComplementToZFlag("B", 1, registers.B); break;
        case BIT_1_C: _copyBitComplementToZFlag("C", 1, registers.C); break;
        case BIT_1_D: _copyBitComplementToZFlag("D", 1, registers.D); break;
        case BIT_1_E: _copyBitComplementToZFlag("E", 1, registers.E); break;
        case BIT_1_H: _copyBitComplementToZFlag("H", 1, registers.H); break;
        case BIT_1_L: _copyBitComplementToZFlag("L", 1, registers.L); break;
        case BIT_1_HL_LOC: _copyBitComplementToZFlag("HL", 1, Address(registers.L, registers.H)); break;

        case BIT_2_A: _copyBitComplementToZFlag("A", 2, registers.A); break;
        case BIT_2_B: _copyBitComplementToZFlag("B", 2, registers.B); break;
        case BIT_2_C: _copyBitComplementToZFlag("C", 2, registers.C); break;
        case BIT_2_D: _copyBitComplementToZFlag("D", 2, registers.D); break;
        case BIT_2_E: _copyBitComplementToZFlag("E", 2, registers.E); break;
        case BIT_2_H: _copyBitComplementToZFlag("H", 2, registers.H); break;
        case BIT_2_L: _copyBitComplementToZFlag("L", 2, registers.L); break;
        case BIT_2_HL_LOC: _copyBitComplementToZFlag("HL", 2, Address(registers.L, registers.H)); break;

        case BIT_3_A: _copyBitComplementToZFlag("A", 3, registers.A); break;
        case BIT_3_B: _copyBitComplementToZFlag("B", 3, registers.B); break;
        case BIT_3_C: _copyBitComplementToZFlag("C", 3, registers.C); break;
        case BIT_3_D: _copyBitComplementToZFlag("D", 3, registers.D); break;
        case BIT_3_E: _copyBitComplementToZFlag("E", 3, registers.E); break;
        case BIT_3_H: _copyBitComplementToZFlag("H", 3, registers.H); break;
        case BIT_3_L: _copyBitComplementToZFlag("L", 3, registers.L); break;
        case BIT_3_HL_LOC: _copyBitComplementToZFlag("HL", 3, Address(registers.L, registers.H)); break;

        case BIT_4_A: _copyBitComplementToZFlag("A", 4, registers.A); break;
        case BIT_4_B: _copyBitComplementToZFlag("B", 4, registers.B); break;
        case BIT_4_C: _copyBitComplementToZFlag("C", 4, registers.C); break;
        case BIT_4_D: _copyBitComplementToZFlag("D", 4, registers.D); break;
        case BIT_4_E: _copyBitComplementToZFlag("E", 4, registers.E); break;
        case BIT_4_H: _copyBitComplementToZFlag("H", 4, registers.H); break;
        case BIT_4_L: _copyBitComplementToZFlag("L", 4, registers.L); break;
        case BIT_4_HL_LOC: _copyBitComplementToZFlag("HL", 4, Address(registers.L, registers.H)); break;

        case BIT_5_A: _copyBitComplementToZFlag("A", 5, registers.A); break;
        case BIT_5_B: _copyBitComplementToZFlag("B", 5, registers.B); break;
        case BIT_5_C: _copyBitComplementToZFlag("C", 5, registers.C); break;
        case BIT_5_D: _copyBitComplementToZFlag("D", 5, registers.D); break;
        case BIT_5_E: _copyBitComplementToZFlag("E", 5, registers.E); break;
        case BIT_5_H: _copyBitComplementToZFlag("H", 5, registers.H); break;
        case BIT_5_L: _copyBitComplementToZFlag("L", 5, registers.L); break;
        case BIT_5_HL_LOC: _copyBitComplementToZFlag("HL", 5, Address(registers.L, registers.H)); break;

        case BIT_6_A: _copyBitComplementToZFlag("A", 6, registers.A); break;
        case BIT_6_B: _copyBitComplementToZFlag("B", 6, registers.B); break;
        case BIT_6_C: _copyBitComplementToZFlag("C", 6, registers.C); break;
        case BIT_6_D: _copyBitComplementToZFlag("D", 6, registers.D); break;
        case BIT_6_E: _copyBitComplementToZFlag("E", 6, registers.E); break;
        case BIT_6_H: _copyBitComplementToZFlag("H", 6, registers.H); break;
        case BIT_6_L: _copyBitComplementToZFlag("L", 6, registers.L); break;
        case BIT_6_HL_LOC: _copyBitComplementToZFlag("HL", 6, Address(registers.L, registers.H)); break;

        case BIT_7_A: _copyBitComplementToZFlag("A", 7, registers.A); break;
        case BIT_7_B: _copyBitComplementToZFlag("B", 7, registers.B); break;
        case BIT_7_C: _copyBitComplementToZFlag("C", 7, registers.C); break;
        case BIT_7_D: _copyBitComplementToZFlag("D", 7, registers.D); break;
        case BIT_7_E: _copyBitComplementToZFlag("E", 7, registers.E); break;
        case BIT_7_H: _copyBitComplementToZFlag("H", 7, registers.H); break;
        case BIT_7_L: _copyBitComplementToZFlag("L", 7, registers.L); break;
        case BIT_7_HL_LOC: _copyBitComplementToZFlag("HL", 7, Address(registers.L, registers.H)); break;

        case SET_0_A: _setBit("A", 0, registers.A); break;
        case SET_0_B: _setBit("B", 0, registers.B); break;
        case SET_0_C: _setBit("C", 0, registers.C); break;
        case SET_0_D: _setBit("D", 0, registers.D); break;
        case SET_0_E: _setBit("E", 0, registers.E); break;
        case SET_0_H: _setBit("H", 0, registers.H); break;
        case SET_0_L: _setBit("L", 0, registers.L); break;
        case SET_0_HL_LOC: _setBit("(HL)", 0, Address(registers.L, registers.H)); break;

        case SET_1_A: _setBit("A", 1, registers.A); break;
        case SET_1_B: _setBit("B", 1, registers.B); break;
        case SET_1_C: _setBit("C", 1, registers.C); break;
        case SET_1_D: _setBit("D", 1, registers.D); break;
        case SET_1_E: _setBit("E", 1, registers.E); break;
        case SET_1_H: _setBit("H", 1, registers.H); break;
        case SET_1_L: _setBit("L", 1, registers.L); break;
        case SET_1_HL_LOC: _setBit("(HL)", 1, Address(registers.L, registers.H)); break;

        case SET_2_A: _setBit("A", 2, registers.A); break;
        case SET_2_B: _setBit("B", 2, registers.B); break;
        case SET_2_C: _setBit("C", 2, registers.C); break;
        case SET_2_D: _setBit("D", 2, registers.D); break;
        case SET_2_E: _setBit("E", 2, registers.E); break;
        case SET_2_H: _setBit("H", 2, registers.H); break;
        case SET_2_L: _setBit("L", 2, registers.L); break;
        case SET_2_HL_LOC: _setBit("(HL)", 2, Address(registers.L, registers.H)); break;

        case SET_3_A: _setBit("A", 3, registers.A); break;
        case SET_3_B: _setBit("B", 3, registers.B); break;
        case SET_3_C: _setBit("C", 3, registers.C); break;
        case SET_3_D: _setBit("D", 3, registers.D); break;
        case SET_3_E: _setBit("E", 3, registers.E); break;
        case SET_3_H: _setBit("H", 3, registers.H); break;
        case SET_3_L: _setBit("L", 3, registers.L); break;
        case SET_3_HL_LOC: _setBit("(HL)", 3, Address(registers.L, registers.H)); break;

        case SET_4_A: _setBit("A", 4, registers.A); break;
        case SET_4_B: _setBit("B", 4, registers.B); break;
        case SET_4_C: _setBit("C", 4, registers.C); break;
        case SET_4_D: _setBit("D", 4, registers.D); break;
        case SET_4_E: _setBit("E", 4, registers.E); break;
        case SET_4_H: _setBit("H", 4, registers.H); break;
        case SET_4_L: _setBit("L", 4, registers.L); break;
        case SET_4_HL_LOC: _setBit("(HL)", 4, Address(registers.L, registers.H)); break;

        case SET_5_A: _setBit("A", 5, registers.A); break;
        case SET_5_B: _setBit("B", 5, registers.B); break;
        case SET_5_C: _setBit("C", 5, registers.C); break;
        case SET_5_D: _setBit("D", 5, registers.D); break;
        case SET_5_E: _setBit("E", 5, registers.E); break;
        case SET_5_H: _setBit("H", 5, registers.H); break;
        case SET_5_L: _setBit("L", 5, registers.L); break;
        case SET_5_HL_LOC: _setBit("(HL)", 5, Address(registers.L, registers.H)); break;

        case SET_6_A: _setBit("A", 6, registers.A); break;
        case SET_6_B: _setBit("B", 6, registers.B); break;
        case SET_6_C: _setBit("C", 6, registers.C); break;
        case SET_6_D: _setBit("D", 6, registers.D); break;
        case SET_6_E: _setBit("E", 6, registers.E); break;
        case SET_6_H: _setBit("H", 6, registers.H); break;
        case SET_6_L: _setBit("L", 6, registers.L); break;
        case SET_6_HL_LOC: _setBit("(HL)", 6, Address(registers.L, registers.H)); break;

        case SET_7_A: _setBit("A", 7, registers.A); break;
        case SET_7_B: _setBit("B", 7, registers.B); break;
        case SET_7_C: _setBit("C", 7, registers.C); break;
        case SET_7_D: _setBit("D", 7, registers.D); break;
        case SET_7_E: _setBit("E", 7, registers.E); break;
        case SET_7_H: _setBit("H", 7, registers.H); break;
        case SET_7_L: _setBit("L", 7, registers.L); break;
        case SET_7_HL_LOC: _setBit("(HL)", 7, Address(registers.L, registers.H)); break;

        case SRL_A: _srl("a", registers.A); break;
        case SRL_B: _srl("b", registers.B); break;
        case SRL_C: _srl("c", registers.C); break;
        case SRL_D: _srl("d", registers.D); break;
        case SRL_E: _srl("e", registers.E); break;
        case SRL_H: _srl("h", registers.H); break;
        case SRL_L: _srl("l", registers.L); break;

        default:
        {   
            LOG("CPU", LOG_ERROR, "Unrecognised op code 0xCB" << std::hex << (int) instruction);
            return false;
        }
    }
    return true;
}

void Cpu::_storeValueToAddress(std::string name, Address address, byte valueRegister)
{
    _mmu->set(address, valueRegister);
    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, name << " - Storing: " << std::hex << (int) valueRegister << " in the location: "
        << std::hex << address.getLocation());
}

void Cpu::_incrementRegister(std::string name, byte& reg)
{
    reg++;
    _increaseCycle();

    _setZFlag((int) reg == 0);
    _setSubtractFlag(false);
    _setHalfCarryFlag((reg & 0x0F) == 0x00);

    LOG("CPU", LOG_DEBUG, name);
}

void Cpu::_decrementRegister(std::string name, byte& reg)
{
    reg--;
    _increaseCycle();

    _setZFlag((int) reg == 0);
    _setSubtractFlag(true);
    _setHalfCarryFlag((reg & 0x0F) == 0x0F);

    LOG("CPU", LOG_DEBUG, name << " register value: " << std::hex << (int) reg);
}

void Cpu::_incrementRegisterPair(byte& register_low, byte& register_high)
{
    auto value = (register_high << 8) | register_low;
    value++;
    register_high = (value >> 8);
    register_low = (value & 0x00FF);
}

void Cpu::_incrementRegisterPairInstruction(std::string name, byte& register_low, byte& register_high)
{
    _increaseCycle(2);
    _incrementRegisterPair(register_low, register_high);
    LOG("CPU", LOG_DEBUG, "INC " << name);
}

void Cpu::_decrementRegisterPairInstruction(std::string name, byte& register_low, byte& register_high)
{
    _increaseCycle(2);
    _decrementRegisterPair(register_low, register_high);
    LOG("CPU", LOG_DEBUG, "DEC " << name);
}

void Cpu::_decrementRegisterPair(byte& register_low, byte& register_high)
{
    auto value = (register_high << 8) | register_low;
    value--;
    register_high = (value >> 8);
    register_low = (value & 0x00FF);
}

void Cpu::_orRegister(std::string name, byte& reg)
{
    registers.A |= reg;
    _increaseCycle();

    _setZFlag((int) registers.A == 0);
    _setSubtractFlag(false);
    _setCarryFlag(false);
    _setHalfCarryFlag(false);

    LOG("CPU", LOG_DEBUG, "OR " << name);
}

void Cpu::_andRegister(std::string name, byte& reg)
{
    registers.A &= reg;
    _increaseCycle();

    _setZFlag((int) registers.A == 0);
    _setSubtractFlag(false);
    _setCarryFlag(false);
    _setHalfCarryFlag(false);

    LOG("CPU", LOG_DEBUG, "AND " << name);
}

void Cpu::_xor(std::string name, byte value)
{
    registers.A ^= value;

    _setZFlag((int) registers.A == 0);
    _setSubtractFlag(false);
    _setCarryFlag(false);
    _setHalfCarryFlag(false);

    _increaseCycle();
    LOG("CPU", LOG_DEBUG, "XOR " << name);
}

void Cpu::_swap(std::string name, byte& reg)
{
    auto lower = reg & 0x0F;
    auto higher = reg & 0xF0;
    reg = (lower << 4) | (higher >> 4);

    _increaseCycle(2);

    _setZFlag((int) reg == 0);
    _setSubtractFlag(false);
    _setCarryFlag(false);
    _setHalfCarryFlag(false);

    LOG("CPU", LOG_DEBUG, "SWAP " << name);
}

void Cpu::_compare(std::string name, byte value)
{
    bool is_equal = (registers.A - value) == 0;

    _setZFlag(is_equal);
    _setSubtractFlag(true);
    _setCarryFlag(registers.A < value);
    _setHalfCarryFlag(((registers.A & 0xF) - (value & 0xF)) < 0);

    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, "CP " << name);
}

void Cpu::_loadRegister(std::string name, byte& reg)
{
    reg = _mmu->next();
    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, name << ", " << std::hex << (int) reg);
}

void Cpu::_loadValueToRegister(std::string name, byte& reg, byte value)
{
    reg = value;
    _increaseCycle();
    LOG("CPU", LOG_DEBUG, name << " - " << std::hex << (int) reg);
}

void Cpu::_loadRegisterPair(std::string name, byte& register_low, byte& register_high)
{
    register_low = _mmu->next();
    register_high = _mmu->next();
    _increaseCycle(3);
    LOG("CPU", LOG_DEBUG, name << ", " << std::hex << (int) register_high << " " << (int) register_low);
}

void Cpu::_restart(std::string name, unsigned int address)
{
    _pushProgramCounterToStack();

    _mmu->setProgramCounter(address);

    _increaseCycle(4);

    LOG("CPU", LOG_DEBUG, "RST " << name);
}

void Cpu::_addRegisters(std::string name, byte& register1, byte& register2)
{
    auto result = register1 + register2;

    register1 = result;

    _setSubtractFlag(false);
    _setZFlag((int) register1 == 0);
    _setCarryFlag((result & 0x100) != 0);
    _setHalfCarryFlag(((register1 & 0xF) + (register2 & 0xF)) > 0xF);

    _increaseCycle();

    LOG("CPU", LOG_DEBUG, "Add " << name);
}

void Cpu::_subtractRegisters(std::string name, byte& register1, byte& register2)
{
    _setCarryFlag(register1 < register2);

    register1 -= register2;

    _setSubtractFlag(true);
    _setZFlag((int) register1 == 0);
    _setHalfCarryFlag(((register1 & 0xF) - (register2 & 0xF)) < 0x0);

    _increaseCycle();

    LOG("CPU", LOG_DEBUG, "Sub " << name);
}

void Cpu::_addRegisterPairs(std::string name, byte& register_low1, byte& register_high1, byte& register_low2,
        byte& register_high2)
{
    auto value1 = (register_high1 << 8) | register_low1;
    auto value2 = (register_high2 << 8) | register_low2;

    auto result = value2 + value1;

    _setSubtractFlag(false);
    _setCarryFlag((result & 0x10000) != 0);

    register_high1 = (result >> 8);
    register_low1 = (result & 0x00FF);

    LOG("CPU", LOG_DEBUG, "ADD " << name);
}

void Cpu::_call(std::string name, byte address_low, byte address_high)
{
    _pushProgramCounterToStack();

    _mmu->setProgramCounter(Address(address_low, address_high));

    _increaseCycle(6);

    LOG("CPU", LOG_DEBUG, name);
}

void Cpu::_resetBit(std::string name, byte bit, byte& reg)
{
    bitwise::setBit(reg, bit, false);
    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, "RES " << name);
}

void Cpu::_resetBit(std::string name, byte bit, Address&& address)
{
    auto b = _mmu->get(address);
    bitwise::setBit(b, bit, false);
    _mmu->set(address, b);
    _increaseCycle(4);
    LOG("CPU", LOG_DEBUG, "RES " << name);
}

void Cpu::_setBit(std::string name, byte bit, byte& reg)
{
    bitwise::setBit(reg, bit, true);
    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, "SET " << name);
}

void Cpu::_setBit(std::string name, byte bit, Address&& address)
{
    auto b = _mmu->get(address);
    bitwise::setBit(b, bit, true);
    _mmu->set(address, b);
    _increaseCycle(4);
    LOG("CPU", LOG_DEBUG, "SET " << name);
}

void Cpu::_copyBitComplementToZFlag(std::string name, byte bit, byte b)
{
    byte complement = ~b;
    auto z_flag = bitwise::getBit(complement, bit);
    _setZFlag(z_flag);
    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, "BIT " << std::dec << (int) bit << ", " << name);   
}

void Cpu::_copyBitComplementToZFlag(std::string name, byte bit, Address address)
{
    _increaseCycle(2);
    auto b = _mmu->get(address);
    _copyBitComplementToZFlag("(" + name + ")", bit, b);
}

void Cpu::_pushStack(byte value)
{
    _mmu->set(Address(registers.P, registers.S), value);
    _decrementRegisterPair(registers.P, registers.S);
}

byte Cpu::_popStack()
{
    _incrementRegisterPair(registers.P, registers.S);
    return _mmu->get(Address(registers.P, registers.S));
}

void Cpu::_pushRegisterPairToStack(std::string name, byte& register_low, byte& register_high)
{
    _pushStack(register_high);
    _pushStack(register_low);

    LOG("CPU", LOG_DEBUG, "PUSH " << name);
}

void Cpu::_popStackToRegisterPair(std::string name, byte& register_low, byte& register_high)
{
    register_low = _popStack();
    register_high = _popStack();

    LOG("CPU", LOG_DEBUG, "POP " << name);
}

void Cpu::_rotateRegisterLeft(std::string name, byte& reg)
{
    auto carry_flag = _getCarryFlag();

    auto will_carry = bitwise::getBit(reg, 7);
    _setCarryFlag(will_carry);

    reg = reg << 1;
    reg |= carry_flag ? 1 : 0;

    _setHalfCarryFlag(false);
    _setSubtractFlag(false);

    _setZFlag((int) reg == 0);

    _increaseCycle(2);

    LOG("CPU", LOG_DEBUG, "RL " << name);
}

void Cpu::_rotateRegisterLeftCarry(std::string name, byte& reg)
{
    auto will_carry = bitwise::getBit(reg, 7);
    _setCarryFlag(will_carry);

    reg = reg << 1;
    reg |= will_carry ? 1 : 0;

    _setHalfCarryFlag(false);
    _setSubtractFlag(false);

    _setZFlag((int) reg == 0);

    _increaseCycle();

    LOG("CPU", LOG_DEBUG, "RLC " << name);
}

void Cpu::_rotateRegisterRightCarry(std::string name, byte& reg)
{
    auto will_carry = bitwise::getBit(reg, 0);
    _setCarryFlag(will_carry);

    reg = reg >> 1;
    reg |= will_carry ? 0b10000000 : 0;

    _setHalfCarryFlag(false);
    _setSubtractFlag(false);

    _setZFlag((int) reg == 0);

    _increaseCycle(1);

    LOG("CPU", LOG_DEBUG, "RR " << name);
}

void Cpu::_slideRegisterLeft(std::string name, byte& reg)
{
    auto will_carry = bitwise::getBit(reg, 7);
    _setCarryFlag(will_carry);

    reg = reg << 1;

    _setHalfCarryFlag(false);
    _setSubtractFlag(false);

    _setZFlag((int) reg == 0);

    _increaseCycle(2);

    LOG("CPU", LOG_DEBUG, "SL " << name);
}

void Cpu::_srl(std::string name, byte& reg)
{
    _setCarryFlag(bitwise::getBit(reg, 0));

    reg = reg >> 1;

    _setHalfCarryFlag(false);
    _setSubtractFlag(false);
    _setZFlag((int) reg == 0);

    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, "SLR " << name);
}

void Cpu::_daa()
{
    auto correction = _getCarryFlag() ? 0x60: 0x00;

    if (_getHalfCarryFlag() || (!_getSubtractFlag() && ((registers.A & 0xF) > 9)))
    {
        correction |= 0x06;
    }

    if (_getCarryFlag() || (!_getSubtractFlag() && (registers.A > 0x99)))
    {
        correction |= 0x60;
    }

    if (_getSubtractFlag())
    {
        registers.A -= correction;
    }
    else
    {
        registers.A += correction;
    }

    if (((correction << 2) & 0x100) != 0)
    {
        _setCarryFlag(true);
    }

    _setHalfCarryFlag(false);
    _setZFlag((int) registers.A == 0);

    _increaseCycle(2);

    LOG("CPU", LOG_DEBUG, "DAA");
}

void Cpu::_addAndCarry(std::string name, byte& reg, Address address)
{
    auto reg2 = _mmu->get(address);
    _addAndCarry(name, reg, reg2);
}

void Cpu::_addAndCarry(std::string name, byte& reg1, byte& reg2)
{
    auto carry = _getCarryFlag();
    auto result = reg1 + reg2 + carry;

    reg1 = result & 0xFF;

    _setZFlag((int) result == 0);
    _setSubtractFlag(false);
    _setCarryFlag(result > 0xFF);
    _setHalfCarryFlag(((reg1 & 0xF) + (reg2 & 0xF) + carry) > 0xF);

    _increaseCycle(2);
    LOG("CPU", LOG_DEBUG, "ADC " << name);
}

void Cpu::_ret(std::string name, std::function<bool()> condition)
{
    _increaseCycle(2);

    if (condition())
    {
        _popStackToProgramCounter();

        _increaseCycle(3);
        LOG("CPU", LOG_DEBUG, "RET " << name);
    }
}

void Cpu::_jump(std::string name, std::function<bool()> condition, bool is_relative)
{
    auto address = _mmu->getProgramCounter();

    // If it's relative it uses the next byte
    // Otherwise it uses the next two bytes
    if (is_relative)
    {
        // We need to get the next byte first to increase the program counter before doing a relative
        // jump
        auto s8 = static_cast<signed char>(_mmu->next());
        address = _mmu->getProgramCounter() + s8;
    }
    else
    {
        auto low = _mmu->next();
        auto high = _mmu->next();
        address = (high << 8) | low;
    }

    _increaseCycle(is_relative ? 3 : 4);

    if (condition())
    {
        LOG("CPU", LOG_DEBUG, (is_relative ? "JR " : "JP " ) << name << " jumping");
        // The s8 is a signed argument
        _mmu->setProgramCounter(address);
    }
    else
    {
        LOG("CPU", LOG_DEBUG, (is_relative ? "JR " : "JP " ) << name << " not jumping");
    }
}

void Cpu::_increaseCycle(unsigned long amount)
{
    _cycle += amount;
}

void Cpu::_setZFlag(bool value)
{
    bitwise::setBit(registers.F, 7, value);
}

bool Cpu::_getZFlag()
{
    return bitwise::getBit(registers.F, 7);
}

bool Cpu::_getCarryFlag()
{
    return bitwise::getBit(registers.F, 4);
}

void Cpu::_setCarryFlag(bool value)
{
    bitwise::setBit(registers.F, 4, value);
}

bool Cpu::_getHalfCarryFlag()
{
    return bitwise::getBit(registers.F, 5);
}

void Cpu::_setHalfCarryFlag(bool value)
{
    bitwise::setBit(registers.F, 5, value);
}

bool Cpu::_getSubtractFlag()
{
    return bitwise::getBit(registers.F, 6);
}

void Cpu::_setSubtractFlag(bool value)
{
    bitwise::setBit(registers.F, 6, value);
}

void Cpu::_pushProgramCounterToStack()
{
    auto program_counter = _mmu->getProgramCounter();

    _pushStack(program_counter >> 8);
    _pushStack(program_counter);
}

void Cpu::_popStackToProgramCounter()
{
    auto low = _popStack();
    auto high = _popStack();

    _mmu->setProgramCounter(Address(low, high));
}