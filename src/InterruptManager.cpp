#include "InterruptManager.h"

#include "Bitwise.h"

InterruptManager::InterruptManager(std::shared_ptr<Mmu> mmu): _mmu(mmu)
{
}

void InterruptManager::setVBlank(bool vBlank)
{
    auto if_flag = _getIF();
    bitwise::setBit(if_flag, 0, vBlank);
    _setIF(if_flag);
}

bool InterruptManager::getVBlank()
{
    auto if_flag = _getIF();
    return bitwise::getBit(if_flag, 0);
}

void InterruptManager::setLCD(bool lcd)
{
    auto if_flag = _getIF();
    bitwise::setBit(if_flag, 1, lcd);
    _setIF(if_flag);
}

bool InterruptManager::getLCD()
{
    auto if_flag = _getIF();
    return bitwise::getBit(if_flag, 1);
}

void InterruptManager::setTimer(bool timer)
{
    auto if_flag = _getIF();
    bitwise::setBit(if_flag, 2, timer);
    _setIF(if_flag);
}

bool InterruptManager::getTimer()
{
    auto if_flag = _getIF();
    return bitwise::getBit(if_flag, 2);
}

void InterruptManager::setSerial(bool serial)
{
    auto if_flag = _getIF();
    bitwise::setBit(if_flag, 3, serial);
    _setIF(if_flag);
}

bool InterruptManager::getSerial()
{
    auto if_flag = _getIF();
    return bitwise::getBit(if_flag, 3);
}

void InterruptManager::setJoypad(bool joypad)
{
    auto if_flag = _getIF();
    bitwise::setBit(if_flag, 4, joypad);
    _setIF(if_flag);
}

bool InterruptManager::getJoypad()
{
    auto if_flag = _getIF();
    return bitwise::getBit(if_flag, 4);
}

void InterruptManager::_setIF(byte value)
{
    _mmu->set(0xFF0F, value);
}

byte InterruptManager::_getIF()
{
    return _mmu->get(0xFF0F);
}