#include "RomReader.h"

#include <fstream>
#include <iterator>

#include "types/Byte.hpp"

rom_data RomReader::read(std::string file_name)
{
    using std::ios;

    std::ifstream file(file_name, ios::binary);

    // Don't read new lines
    file.unsetf(ios::skipws);

    // Get the file size
    file.seekg(0, ios::end);
    std::streampos fileSize = file.tellg();

    // Go back to the start
    file.seekg(0, ios::beg);

    rom_data data {};
    data.reserve(fileSize);

    data.insert(data.begin(),
        std::istream_iterator<byte>(file),
        std::istream_iterator<byte>());

    return data;
}