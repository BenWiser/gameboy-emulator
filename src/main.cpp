#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include <algorithm>
#include <memory>

#include "Timer.h"
#include "Log.hpp"
#include "Joypad.h"
#include "RomReader.h"
#include "Cpu.h"
#include "Gpu.h"
#include "Constants.hpp"

namespace controls
{
Joypad* joypad;
void onKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods);
} // namespace joypad

int main()
{
    LOG("MAIN", LOG_INFO, "Starting Gameboy emulator");

    auto biosData = RomReader::read("./assets/dmg_boot.bin");
    auto romData = RomReader::read("./assets/tetris.gb");

    auto mmu = std::make_shared<Mmu>(std::move(biosData), std::move(romData));
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Timer timer(mmu);
    controls::joypad = new Joypad(interrupt_manager, mmu);
    Cpu cpu(interrupt_manager, mmu);
    Gpu gpu(interrupt_manager, mmu);

    if (!glfwInit())
    {
        LOG("GLFW", LOG_ERROR, "Failed to initialise");
        return 1;
    }

    auto window = glfwCreateWindow(constants::WINDOW_WIDTH, constants::WINDOW_HEIGHT,
        "Bens Groovy Gameboy Emulator", NULL, NULL);

    glfwMakeContextCurrent(window);

    glClearColor(255.0f, 255.0f, 255.0f, 1.0f);

    auto isRunning = true;

    unsigned long cycle = 0;

    auto emulate = [&]() {
        controls::joypad->tick();

        isRunning = cpu.tick(cycle);
        if (!isRunning)
        {
            LOG("MAIN", LOG_ERROR, "Error detected with CPU - halting execution");
        }

        timer.tick(cycle);

        bool shouldRender = gpu.tick(cycle);

        if (shouldRender)
        {
            glfwSwapBuffers(window);
        }
    };

    glfwSetKeyCallback(window, controls::onKeyPress);

    while (!glfwWindowShouldClose(window))
    {
        // This doesn't get past the boot loader right now so simply stop executing the emulator 
        // when this should stop running
        if (isRunning)
        {
            emulate();
        }
        glfwPollEvents();
    }

    LOG("MAIN", LOG_INFO, "Shutting down Gameboy emulator");

    glfwTerminate();

    delete controls::joypad;

    return 0;
}

void controls::onKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (joypad)
    {
        switch (key)
        {
            case GLFW_KEY_LEFT:
            {
                joypad->setLeft(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
            case GLFW_KEY_RIGHT:
            {
                joypad->setRight(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
            case GLFW_KEY_UP:
            {
                joypad->setUp(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
            case GLFW_KEY_DOWN:
            {
                joypad->setDown(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
            case GLFW_KEY_Z:
            {
                joypad->setA(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
            case GLFW_KEY_X:
            {
                joypad->setB(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
            case GLFW_KEY_ENTER:
            {
                joypad->setStart(action == GLFW_PRESS || action == GLFW_REPEAT);
                break;
            }
        }
    }
}