
#include "gmock/gmock.h"

#include "types/RomData.hpp"
#include "types/Byte.hpp"
#include "InterruptManager.h"
#include "OpCodes.hpp"
#include "Mmu.h"
#include "Cpu.h"

TEST(MathOpcodes, AddOpcodes)
{
    rom_data test_data {
        ADD_A_A,
        ADD_A_B,
        ADD_A_C,
        ADD_A_D,
        ADD_A_E,
        ADD_A_H,
        ADD_A_L,
        ADD_A_HL_LOC,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 1;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 1);
    ASSERT_EQ(2, cpu.registers.A);

    cpu.registers.A = 1;
    cpu.registers.B = 2;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 2);
    ASSERT_EQ(3, cpu.registers.A);
    ASSERT_EQ(2, cpu.registers.B);

    cpu.registers.A = 2;
    cpu.registers.C = 3;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 3);
    ASSERT_EQ(5, cpu.registers.A);
    ASSERT_EQ(3, cpu.registers.C);

    cpu.registers.A = 10;
    cpu.registers.D = 1;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 4);
    ASSERT_EQ(11, cpu.registers.A);
    ASSERT_EQ(1, cpu.registers.D);

    cpu.registers.A = 3;
    cpu.registers.E = 9;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 5);
    ASSERT_EQ(12, cpu.registers.A);
    ASSERT_EQ(9, cpu.registers.E);

    cpu.registers.A = 5;
    cpu.registers.H = 5;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 6);
    ASSERT_EQ(10, cpu.registers.A);
    ASSERT_EQ(5, cpu.registers.H);

    cpu.registers.A = 3;
    cpu.registers.L = 3;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 7);
    ASSERT_EQ(6, cpu.registers.A);
    ASSERT_EQ(3, cpu.registers.L);

    cpu.registers.A = 3;
    cpu.registers.H = 0xFF;
    cpu.registers.L = 0x08;
    mmu->set(0xFF08, 2);

    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 9);
    ASSERT_EQ(5, cpu.registers.A);
}

TEST(MathOpcodes, AddPairsOpcodes)
{
    rom_data test_data {
        ADD_HL_BC,
        ADD_HL_DE,
        ADD_HL_HL,
        ADD_HL_SP
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.H = 0x01;
    cpu.registers.L = 0x02;
    cpu.registers.B = 0x00;
    cpu.registers.C = 0x01;

    cpu.tick(cycle);
    ASSERT_EQ(2, cycle);
    ASSERT_EQ(0x01, cpu.registers.H);
    ASSERT_EQ(0x03, cpu.registers.L);

    cpu.registers.H = 0x01;
    cpu.registers.L = 0x02;
    cpu.registers.D = 0x01;
    cpu.registers.E = 0x02;

    cpu.tick(cycle);
    ASSERT_EQ(4, cycle);
    ASSERT_EQ(0x02, cpu.registers.H);
    ASSERT_EQ(0x04, cpu.registers.L);

    cpu.registers.H = 0x01;
    cpu.registers.L = 0x02;

    cpu.tick(cycle);
    ASSERT_EQ(6, cycle);
    ASSERT_EQ(0x02, cpu.registers.H);
    ASSERT_EQ(0x04, cpu.registers.L);

    cpu.registers.H = 0x0A;
    cpu.registers.L = 0x10;
    cpu.registers.S = 0x02;
    cpu.registers.P = 0x00;

    cpu.tick(cycle);
    ASSERT_EQ(8, cycle);
    ASSERT_EQ(0x0C, cpu.registers.H);
    ASSERT_EQ(0x10, cpu.registers.L);
}

TEST(MathOpcodes, SubOpcodes)
{
    rom_data test_data {
        SUB_A,
        SUB_B,
        SUB_C,
        SUB_D,
        SUB_E,
        SUB_H,
        SUB_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 1;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 1);
    ASSERT_EQ(0, cpu.registers.A);

    cpu.registers.A = 2;
    cpu.registers.B = 1;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 2);
    ASSERT_EQ(1, cpu.registers.A);
    ASSERT_EQ(1, cpu.registers.B);

    cpu.registers.A = 5;
    cpu.registers.C = 3;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 3);
    ASSERT_EQ(2, cpu.registers.A);
    ASSERT_EQ(3, cpu.registers.C);

    cpu.registers.A = 10;
    cpu.registers.D = 1;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 4);
    ASSERT_EQ(9, cpu.registers.A);
    ASSERT_EQ(1, cpu.registers.D);

    cpu.registers.A = 9;
    cpu.registers.E = 3;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 5);
    ASSERT_EQ(6, cpu.registers.A);
    ASSERT_EQ(3, cpu.registers.E);

    cpu.registers.A = 5;
    cpu.registers.H = 5;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 6);
    ASSERT_EQ(0, cpu.registers.A);
    ASSERT_EQ(5, cpu.registers.H);

    cpu.registers.A = 7;
    cpu.registers.L = 3;
    cpu.tick(cycle);
    ASSERT_TRUE(cycle == 7);
    ASSERT_EQ(4, cpu.registers.A);
    ASSERT_EQ(3, cpu.registers.L);
}

TEST(MathOpcodes, IncOpcodes)
{
    rom_data test_data {
        INC_A,
        INC_B,
        INC_C,
        INC_D,
        INC_E,
        INC_H,
        INC_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 1;
        cpu.tick(cycle);
        ASSERT_TRUE(cycle == 1);
        ASSERT_EQ(2, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(MathOpcodes, IncPairOpcodes)
{
    rom_data test_data {
        INC_BC,
        INC_DE,
        INC_HL,
        INC_SP,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register_pair = [&](byte& reg_high, byte& reg_low) {
        cycle = 0;
        
        reg_high = 0xF1;
        reg_low = 0x02;

        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        ASSERT_EQ(0xF1, reg_high);
        ASSERT_EQ(0x03, reg_low);
    };

    test_register_pair(cpu.registers.B, cpu.registers.C);
    test_register_pair(cpu.registers.D, cpu.registers.E);
    test_register_pair(cpu.registers.H, cpu.registers.L);
    test_register_pair(cpu.registers.S, cpu.registers.P);
}

TEST(MathOpcodes, DecOpcodes)
{
    rom_data test_data {
        DEC_A,
        DEC_B,
        DEC_C,
        DEC_D,
        DEC_E,
        DEC_H,
        DEC_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 2;
        cpu.tick(cycle);
        ASSERT_TRUE(cycle == 1);
        ASSERT_EQ(1, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(MathOpcodes, DecPairOpcodes)
{
    rom_data test_data {
        DEC_BC,
        DEC_DE,
        DEC_HL,
        DEC_SP,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register_pair = [&](byte& reg_high, byte& reg_low) {
        cycle = 0;
        
        reg_high = 0xF1;
        reg_low = 0x02;

        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        ASSERT_EQ(0xF1, reg_high);
        ASSERT_EQ(0x01, reg_low);
    };

    test_register_pair(cpu.registers.B, cpu.registers.C);
    test_register_pair(cpu.registers.D, cpu.registers.E);
    test_register_pair(cpu.registers.H, cpu.registers.L);
    test_register_pair(cpu.registers.S, cpu.registers.P);
}

TEST(MathOpcodes, Inc_HL_Location_Contents)
{
    rom_data test_data {
        INC_HL_LOC,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cycle = 0;
    
    byte low = 0x04;
    byte high = 0xF2;

    cpu.registers.H = high;
    cpu.registers.L = low;

    Address address(low, high);

    mmu->set(address, 0x03);

    cpu.tick(cycle);
    ASSERT_EQ(3, cycle);
    ASSERT_EQ(0x04, mmu->get(address));
}

TEST(MathOpcodes, Dec_HL_Location_Contents)
{
    rom_data test_data {
        DEC_HL_LOC,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cycle = 0;
    
    byte low = 0x04;
    byte high = 0xF2;

    cpu.registers.H = high;
    cpu.registers.L = low;

    Address address(low, high);

    mmu->set(address, 0x03);

    cpu.tick(cycle);
    ASSERT_EQ(3, cycle);
    ASSERT_EQ(0x02, mmu->get(address));
}