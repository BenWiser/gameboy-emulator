#include "gmock/gmock.h"

#include "types/Address.h"
#include "types/Byte.hpp"
#include "types/RomData.hpp"
#include "InterruptManager.h"
#include "OpCodes.hpp"
#include "Mmu.h"
#include "Cpu.h"

TEST(InterruptsTest, Interrupts_Jump_To_Correct_Locations)
{
    rom_data test_data {
        EI,
        NOP,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_interrupt = [&](byte flag, unsigned int location) {
        mmu->set(0xFFFF, flag);

        cycle = 0;
        mmu->setProgramCounter(0);
        cpu.tick(cycle);

        cpu.registers.S = 0xFF;
        cpu.registers.P = 0x07;

        cpu.tick(cycle);

        ASSERT_EQ(location, mmu->getProgramCounter());
        ASSERT_EQ(0xFF, cpu.registers.S);
        ASSERT_EQ(0x05, cpu.registers.P);
        mmu->set(0xFFFF, 0x00);
    };

    interrupt_manager->setVBlank(true);
    test_interrupt(0b00000001, 0x0041);

    interrupt_manager->setLCD(true);
    test_interrupt(0b00000010, 0x0049);

    interrupt_manager->setTimer(true);
    test_interrupt(0b00000100, 0x0051);

    interrupt_manager->setSerial(true);
    test_interrupt(0b00001000, 0x0059);

    interrupt_manager->setJoypad(true);
    test_interrupt(0b00010000, 0x0061);
}