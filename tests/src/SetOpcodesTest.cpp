
#include "gmock/gmock.h"

#include "types/RomData.hpp"
#include "types/Byte.hpp"
#include "InterruptManager.h"
#include "OpCodes.hpp"
#include "Mmu.h"
#include "Cpu.h"

TEST(Opcodes, Set_0_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_0_A,
        SIXTEEN_BIT_OPCODES,
        SET_0_B,
        SIXTEEN_BIT_OPCODES,
        SET_0_C,
        SIXTEEN_BIT_OPCODES,
        SET_0_D,
        SIXTEEN_BIT_OPCODES,
        SET_0_E,
        SIXTEEN_BIT_OPCODES,
        SET_0_H,
        SIXTEEN_BIT_OPCODES,
        SET_0_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01001010;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01001011, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_1_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_1_A,
        SIXTEEN_BIT_OPCODES,
        SET_1_B,
        SIXTEEN_BIT_OPCODES,
        SET_1_C,
        SIXTEEN_BIT_OPCODES,
        SET_1_D,
        SIXTEEN_BIT_OPCODES,
        SET_1_E,
        SIXTEEN_BIT_OPCODES,
        SET_1_H,
        SIXTEEN_BIT_OPCODES,
        SET_1_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01001001;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01001011, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_2_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_2_A,
        SIXTEEN_BIT_OPCODES,
        SET_2_B,
        SIXTEEN_BIT_OPCODES,
        SET_2_C,
        SIXTEEN_BIT_OPCODES,
        SET_2_D,
        SIXTEEN_BIT_OPCODES,
        SET_2_E,
        SIXTEEN_BIT_OPCODES,
        SET_2_H,
        SIXTEEN_BIT_OPCODES,
        SET_2_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01001010;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01001110, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_3_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_3_A,
        SIXTEEN_BIT_OPCODES,
        SET_3_B,
        SIXTEEN_BIT_OPCODES,
        SET_3_C,
        SIXTEEN_BIT_OPCODES,
        SET_3_D,
        SIXTEEN_BIT_OPCODES,
        SET_3_E,
        SIXTEEN_BIT_OPCODES,
        SET_3_H,
        SIXTEEN_BIT_OPCODES,
        SET_3_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01000110;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01001110, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_4_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_4_A,
        SIXTEEN_BIT_OPCODES,
        SET_4_B,
        SIXTEEN_BIT_OPCODES,
        SET_4_C,
        SIXTEEN_BIT_OPCODES,
        SET_4_D,
        SIXTEEN_BIT_OPCODES,
        SET_4_E,
        SIXTEEN_BIT_OPCODES,
        SET_4_H,
        SIXTEEN_BIT_OPCODES,
        SET_4_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01000110;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01010110, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_5_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_5_A,
        SIXTEEN_BIT_OPCODES,
        SET_5_B,
        SIXTEEN_BIT_OPCODES,
        SET_5_C,
        SIXTEEN_BIT_OPCODES,
        SET_5_D,
        SIXTEEN_BIT_OPCODES,
        SET_5_E,
        SIXTEEN_BIT_OPCODES,
        SET_5_H,
        SIXTEEN_BIT_OPCODES,
        SET_5_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01000110;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01100110, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_6_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_6_A,
        SIXTEEN_BIT_OPCODES,
        SET_6_B,
        SIXTEEN_BIT_OPCODES,
        SET_6_C,
        SIXTEEN_BIT_OPCODES,
        SET_6_D,
        SIXTEEN_BIT_OPCODES,
        SET_6_E,
        SIXTEEN_BIT_OPCODES,
        SET_6_H,
        SIXTEEN_BIT_OPCODES,
        SET_6_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b00100110;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b01100110, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(Opcodes, Set_7_Opcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SET_7_A,
        SIXTEEN_BIT_OPCODES,
        SET_7_B,
        SIXTEEN_BIT_OPCODES,
        SET_7_C,
        SIXTEEN_BIT_OPCODES,
        SET_7_D,
        SIXTEEN_BIT_OPCODES,
        SET_7_E,
        SIXTEEN_BIT_OPCODES,
        SET_7_H,
        SIXTEEN_BIT_OPCODES,
        SET_7_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01100110;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b11100110, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}