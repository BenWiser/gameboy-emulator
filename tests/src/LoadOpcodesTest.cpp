#include "gmock/gmock.h"

#include "types/Address.h"
#include "types/Byte.hpp"
#include "types/RomData.hpp"
#include "InterruptManager.h"
#include "OpCodes.hpp"
#include "Mmu.h"
#include "Cpu.h"

TEST(LoadOpcodesTest, LoadPairs)
{
    rom_data test_data {
        LD_BC,
        0x01,
        0x02,
        LD_DE,
        0x01,
        0x02,
        LD_SP,
        0x01,
        0x02,
        LD_HL,
        0x01,
        0x02
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg_low, byte& reg_high) {
        cycle = 0;
        cpu.tick(cycle);
        ASSERT_EQ(3, cycle);
        
        ASSERT_EQ(0x01, reg_low);
        ASSERT_EQ(0x02, reg_high);
    };

    test_register(cpu.registers.C, cpu.registers.B);
    test_register(cpu.registers.E, cpu.registers.D);
    test_register(cpu.registers.P, cpu.registers.S);
    test_register(cpu.registers.L, cpu.registers.H);
}

TEST(LoadOpcodesTest, LoadRegister)
{
    rom_data test_data {
        LD_A,
        0x01,
        LD_B,
        0x01,
        LD_C,
        0x01,
        LD_D,
        0x01,
        LD_E,
        0x01,
        LD_H,
        0x01,
        LD_L,
        0x01,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0x01, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(LoadOpcodesTest, LoadRegisterToAnother)
{
    rom_data test_data {
        LD_A_A,
        LD_A_B,
        LD_A_C,
        LD_A_D,
        LD_A_E,
        LD_A_H,
        LD_A_L,

        LD_B_A,
        LD_B_B,
        LD_B_C,
        LD_B_D,
        LD_B_E,
        LD_B_H,
        LD_B_L,

        LD_C_A,
        LD_C_B,
        LD_C_C,
        LD_C_D,
        LD_C_E,
        LD_C_H,
        LD_C_L,

        LD_D_A,
        LD_D_B,
        LD_D_C,
        LD_D_D,
        LD_D_E,
        LD_D_H,
        LD_D_L,

        LD_E_A,
        LD_E_B,
        LD_E_C,
        LD_E_D,
        LD_E_E,
        LD_E_H,
        LD_E_L,

        LD_H_A,
        LD_H_B,
        LD_H_C,
        LD_H_D,
        LD_H_E,
        LD_H_H,
        LD_H_L,

        LD_L_A,
        LD_L_B,
        LD_L_C,
        LD_L_D,
        LD_L_E,
        LD_L_H,
        LD_L_L,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg_1, byte& reg_2) {
        cycle = 0;
        reg_1 = 0x00;
        reg_2 = 0x02;
        cpu.tick(cycle);

        ASSERT_EQ(1, cycle);
        ASSERT_EQ(0x02, reg_1);
    };

    test_register(cpu.registers.A, cpu.registers.A);
    test_register(cpu.registers.A, cpu.registers.B);
    test_register(cpu.registers.A, cpu.registers.C);
    test_register(cpu.registers.A, cpu.registers.D);
    test_register(cpu.registers.A, cpu.registers.E);
    test_register(cpu.registers.A, cpu.registers.H);
    test_register(cpu.registers.A, cpu.registers.L);

    test_register(cpu.registers.B, cpu.registers.A);
    test_register(cpu.registers.B, cpu.registers.B);
    test_register(cpu.registers.B, cpu.registers.C);
    test_register(cpu.registers.B, cpu.registers.D);
    test_register(cpu.registers.B, cpu.registers.E);
    test_register(cpu.registers.B, cpu.registers.H);
    test_register(cpu.registers.B, cpu.registers.L);

    test_register(cpu.registers.C, cpu.registers.A);
    test_register(cpu.registers.C, cpu.registers.B);
    test_register(cpu.registers.C, cpu.registers.C);
    test_register(cpu.registers.C, cpu.registers.D);
    test_register(cpu.registers.C, cpu.registers.E);
    test_register(cpu.registers.C, cpu.registers.H);
    test_register(cpu.registers.C, cpu.registers.L);

    test_register(cpu.registers.D, cpu.registers.A);
    test_register(cpu.registers.D, cpu.registers.B);
    test_register(cpu.registers.D, cpu.registers.C);
    test_register(cpu.registers.D, cpu.registers.D);
    test_register(cpu.registers.D, cpu.registers.E);
    test_register(cpu.registers.D, cpu.registers.H);
    test_register(cpu.registers.D, cpu.registers.L);

    test_register(cpu.registers.E, cpu.registers.A);
    test_register(cpu.registers.E, cpu.registers.B);
    test_register(cpu.registers.E, cpu.registers.C);
    test_register(cpu.registers.E, cpu.registers.D);
    test_register(cpu.registers.E, cpu.registers.E);
    test_register(cpu.registers.E, cpu.registers.H);
    test_register(cpu.registers.E, cpu.registers.L);

    test_register(cpu.registers.H, cpu.registers.A);
    test_register(cpu.registers.H, cpu.registers.B);
    test_register(cpu.registers.H, cpu.registers.C);
    test_register(cpu.registers.H, cpu.registers.D);
    test_register(cpu.registers.H, cpu.registers.E);
    test_register(cpu.registers.H, cpu.registers.H);
    test_register(cpu.registers.H, cpu.registers.L);

    test_register(cpu.registers.L, cpu.registers.A);
    test_register(cpu.registers.L, cpu.registers.B);
    test_register(cpu.registers.L, cpu.registers.C);
    test_register(cpu.registers.L, cpu.registers.D);
    test_register(cpu.registers.L, cpu.registers.E);
    test_register(cpu.registers.L, cpu.registers.H);
    test_register(cpu.registers.L, cpu.registers.L);
}

TEST(LoadOpcodesTest, LoadToRam_a8)
{
    byte offset { 0x02 };
    rom_data test_data { LD_A_LOC, offset };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0x04;

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(3, cycle);

    ASSERT_EQ(mmu->get(0xFF00 + (signed int) offset), 0x04);
}

TEST(LoadOpcodesTest, LoadFromRam_a8_to_A)
{
    byte offset { 0x02 };
    rom_data test_data { LD_A_a8_LOC, offset };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0x00;

    mmu->set(0xFF00 + (signed int) offset, 0x04);

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(3, cycle);

    ASSERT_EQ(0x04, cpu.registers.A);
}

TEST(LoadOpcodesTest, LoadToRam_a16)
{
    byte address_low { 0x02 };
    byte address_high { 0xff };
    rom_data test_data { LD_a16_LOC_A, address_low, address_high };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0x04;

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(4, cycle);

    ASSERT_EQ(mmu->get(Address(address_low, address_high)), 0x04);
}

TEST(LoadOpcodesTest, LoadFromRam_a16_to_A)
{
    byte address_low { 0x02 };
    byte address_high { 0xff };
    rom_data test_data { LD_A_a16_LOC, address_low, address_high };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0x00;

    mmu->set(Address(address_low, address_high), 0x04);

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(4, cycle);

    ASSERT_EQ(0x04, cpu.registers.A);
}

TEST(LoadOpcodesTest, LoadToRam_a16_StackPointer)
{
    byte address_low { 0x02 };
    byte address_high { 0xf1 };
    rom_data test_data { LD_a16_LOC_SP, address_low, address_high };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};
    Address address(address_low, address_high);

    cpu.registers.P = 0x05;
    cpu.registers.S = 0x04;

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(5, cycle);

    ASSERT_EQ(mmu->get(address), 0x05);
    ASSERT_EQ(mmu->get(address + 1), 0x04);
}

TEST(LoadOpcodesTest, Load_Value_To_Location_Specified_C)
{
    rom_data test_data { LD_C_LOC_A };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    byte offset = 0x05;
    byte value = 0x02;

    cpu.registers.C = offset;
    cpu.registers.A = value;

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(2, cycle);

    ASSERT_EQ(mmu->get(0xFF00 + offset), value);
}

TEST(LoadOpcodesTest, Load_Value_To_Location_Specified_By_Register_Pair)
{
    byte value = 0x02;
    byte low = 0x02;
    byte high = 0xF2;

    rom_data test_data {
        LD_HL_LOC_A,
        LD_HL_LOC_B,
        LD_HL_LOC_C,
        LD_HL_LOC_D,
        LD_HL_LOC_E,
        LD_HL_LOC_H,
        LD_HL_LOC_L,
        LD_DE_LOC_A,
        LD_HL_LOC_d8,
        value,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg_low, byte& reg_high, byte& reg) {
        Address address(low, high);
        reg = value;

        reg_low = low;
        reg_high = high;
        mmu->set(address, 0x00);
        cycle = 0;
        cpu.tick(cycle);

        ASSERT_EQ(2, cycle);
        ASSERT_EQ(mmu->get(address), reg);
    };

    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.A);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.B);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.C);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.D);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.E);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.H);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.L);
    test_register(cpu.registers.E, cpu.registers.D, cpu.registers.A);

    Address address(low, high);

    cpu.registers.H = high;
    cpu.registers.L = low;
    mmu->set(address, 0x00);
    cycle = 0;
    cpu.tick(cycle);

    ASSERT_EQ(3, cycle);
    ASSERT_EQ(mmu->get(address), value);
}

TEST(LoadOpcodesTest, Load_Contents_Of_Register_Pair_Location_To_Register)
{
    rom_data test_data {
        LD_A_DE_LOC,
        LD_A_HL_LOC,
        LD_B_HL_LOC,
        LD_C_HL_LOC,
        LD_D_HL_LOC,
        LD_E_HL_LOC,
        LD_H_HL_LOC,
        LD_L_HL_LOC,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    byte value = 0x02;
    byte low_address = 0x02;
    byte high_address = 0xF2;

    Address address(low_address, high_address);
    mmu->set(address, value);

    auto test_register = [&](byte& reg_low, byte& reg_high, byte& reg) {
        reg_low = low_address;
        reg_high = high_address;

        cycle = 0;
        cpu.tick(cycle);

        ASSERT_EQ(2, cycle);
        ASSERT_EQ(value, reg);
    };

    test_register(cpu.registers.E, cpu.registers.D, cpu.registers.A);

    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.A);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.B);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.C);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.D);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.E);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.H);
    test_register(cpu.registers.L, cpu.registers.H, cpu.registers.L);
}

TEST(LoadOpcodesTest, Load_Register_And_Decrement_HL)
{
    rom_data test_data {
        LD_HL_MINUS_A,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.H = 0xFF;
    cpu.registers.L = 0x02;

    cpu.registers.A = 0x04;

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(2, cycle);
    
    ASSERT_EQ(cpu.registers.H, 0xFF);
    ASSERT_EQ(cpu.registers.L, 0x01);
    ASSERT_EQ(mmu->get(0xFF02), 0x04);
}

TEST(LoadOpcodesTest, Load_Register_And_Increment_HL)
{
    rom_data test_data {
        LD_HL_PLUS_A,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.H = 0xFF;
    cpu.registers.L = 0x02;

    cpu.registers.A = 0x04;

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(2, cycle);
    
    ASSERT_EQ(cpu.registers.H, 0xFF);
    ASSERT_EQ(cpu.registers.L, 0x03);
    ASSERT_EQ(mmu->get(0xFF02), 0x04);
}

TEST(LoadOpcodesTest, Load_HL_Contents_And_Increment)
{
    rom_data test_data {
        LD_A_HL_PLUS,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.H = 0xFF;
    cpu.registers.L = 0x02;

    mmu->set(0xFF02, 0x04);

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(2, cycle);
    
    ASSERT_EQ(cpu.registers.H, 0xFF);
    ASSERT_EQ(cpu.registers.L, 0x03);
    ASSERT_EQ(0x04, cpu.registers.A);
}

TEST(LoadOpcodesTest, Load_HL_Contents_And_Decrement)
{
    rom_data test_data {
        LD_A_HL_MINUS,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.H = 0xFF;
    cpu.registers.L = 0x02;

    mmu->set(0xFF02, 0x04);

    cycle = 0;
    cpu.tick(cycle);
    ASSERT_EQ(2, cycle);
    
    ASSERT_EQ(cpu.registers.H, 0xFF);
    ASSERT_EQ(cpu.registers.L, 0x01);
    ASSERT_EQ(0x04, cpu.registers.A);
}