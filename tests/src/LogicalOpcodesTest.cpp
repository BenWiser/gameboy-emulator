
#include "gmock/gmock.h"

#include "types/RomData.hpp"
#include "types/Byte.hpp"
#include "InterruptManager.h"
#include "Bitwise.h"
#include "OpCodes.hpp"
#include "Mmu.h"
#include "Cpu.h"

TEST(LogicalOpcodes, OrOpcodes)
{
    rom_data test_data {
        OR_B,
        OR_C,
        OR_D,
        OR_E,
        OR_H,
        OR_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        byte first = 0b00101010;
        byte second = 0b00101001;
        cycle = 0;
        cpu.registers.A = first;
        reg = second;
        cpu.tick(cycle);
        ASSERT_TRUE(cycle == 1);
        
        ASSERT_EQ(first | second, cpu.registers.A);
    };

    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(LogicalOpcodes, AndOpcodes)
{
    rom_data test_data {
        AND_B,
        AND_C,
        AND_D,
        AND_E,
        AND_H,
        AND_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        byte first = 0b00101010;
        byte second = 0b00101001;
        cycle = 0;
        cpu.registers.A = first;
        reg = second;
        cpu.tick(cycle);
        ASSERT_TRUE(cycle == 1);
        
        ASSERT_EQ(first & second, cpu.registers.A);
    };

    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(LogicalOpcodes, And_d8_Opcode)
{
    rom_data test_data {
        AND_d8,
        0b00001001,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0b01011001;
    cpu.tick(cycle);

    EXPECT_EQ(2, cycle);
    EXPECT_EQ(0b00001001, cpu.registers.A);
}

TEST(LogicalOpcodes, Or_d8_Opcode)
{
    rom_data test_data {
        OR_d8,
        0b10001000,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0b01011001;
    cpu.tick(cycle);

    EXPECT_EQ(2, cycle);
    EXPECT_EQ(0b11011001, cpu.registers.A);
}

TEST(LogicalOpcodes, XorOpcodes)
{
    rom_data test_data {
        XOR_B,
        XOR_C,
        XOR_D,
        XOR_E,
        XOR_H,
        XOR_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        byte first = 0b00101010;
        byte second = 0b00101001;
        cycle = 0;
        cpu.registers.A = first;
        reg = second;
        cpu.tick(cycle);
        ASSERT_TRUE(cycle == 1);
        
        ASSERT_EQ(first ^ second, cpu.registers.A);
    };

    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(LogicalOpcodes, Comparisons)
{
    rom_data test_data {
        CP_d8,
        0x02,

        CP_d8,
        0x04,

        CP_HL_LOC,
        CP_HL_LOC,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0x02;
    cpu.tick(cycle);

    EXPECT_EQ(2, cycle);
    EXPECT_EQ(true, bitwise::getBit(cpu.registers.F, 7));

    cpu.registers.A = 0x02;
    cpu.tick(cycle);

    EXPECT_EQ(4, cycle);
    EXPECT_EQ(false, bitwise::getBit(cpu.registers.F, 7));

    byte low = 0x02;
    byte high = 0xF2;
    Address address(low, high);

    mmu->set(address, 0x02);
    cpu.registers.A = 0x02;

    cpu.registers.H = high;
    cpu.registers.L = low;

    cpu.tick(cycle);

    EXPECT_EQ(6, cycle);
    EXPECT_EQ(true, bitwise::getBit(cpu.registers.F, 7));

    mmu->set(address, 0x04);
    cpu.registers.A = 0x02;

    cpu.tick(cycle);

    EXPECT_EQ(8, cycle);
    EXPECT_EQ(false, bitwise::getBit(cpu.registers.F, 7));
}