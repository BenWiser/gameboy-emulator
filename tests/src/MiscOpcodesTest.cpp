#include "gmock/gmock.h"

#include "types/RomData.hpp"
#include "types/Byte.hpp"
#include "InterruptManager.h"
#include "OpCodes.hpp"
#include "Mmu.h"
#include "Cpu.h"

TEST(MiscOpcodes, SwapOpcodes)
{
    rom_data test_data {
        SIXTEEN_BIT_OPCODES,
        SWAP_A,
        SIXTEEN_BIT_OPCODES,
        SWAP_B,
        SIXTEEN_BIT_OPCODES,
        SWAP_C,
        SIXTEEN_BIT_OPCODES,
        SWAP_D,
        SIXTEEN_BIT_OPCODES,
        SWAP_E,
        SIXTEEN_BIT_OPCODES,
        SWAP_H,
        SIXTEEN_BIT_OPCODES,
        SWAP_L
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    auto test_register = [&](byte& reg) {
        cycle = 0;
        reg = 0b01001011;
        cpu.tick(cycle);
        ASSERT_EQ(2, cycle);
        
        ASSERT_EQ(0b10110100, reg);
    };

    test_register(cpu.registers.A);
    test_register(cpu.registers.B);
    test_register(cpu.registers.C);
    test_register(cpu.registers.D);
    test_register(cpu.registers.E);
    test_register(cpu.registers.H);
    test_register(cpu.registers.L);
}

TEST(MiscOpcodes, One_Compliments_Of_Register_A)
{
    rom_data test_data {
        CPL,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    unsigned long cycle {0};

    cpu.registers.A = 0b00101001;

    cpu.tick(cycle);

    ASSERT_EQ(1, cycle);
    ASSERT_EQ(0b11010110, cpu.registers.A);
}

TEST(MiscOpcodes, Call)
{
    rom_data test_data {
        CALL,
        0x02,
        0xA2,
    };

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Cpu cpu(interrupt_manager, mmu);

    cpu.registers.S = 0xFF;
    cpu.registers.P = 0x07;

    unsigned long cycle {0};

    cpu.tick(cycle);

    ASSERT_EQ(6, cycle);
    ASSERT_EQ(0xA202, mmu->getProgramCounter());
    ASSERT_EQ(0xFF, cpu.registers.S);
    ASSERT_EQ(0x05, cpu.registers.P);
    ASSERT_EQ(0x00, mmu->get(0xFF07));
    ASSERT_EQ(0x03, mmu->get(0xFF06));
}