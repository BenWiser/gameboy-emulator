#include "gmock/gmock.h"

#include "types/Address.h"
#include "types/Byte.hpp"
#include "types/RomData.hpp"
#include "Mmu.h"

TEST(DMATest, DMA_Transfers_Data_To_OAM)
{
    rom_data test_data {};

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});

    Address address(0xFA00);
    byte test_value = 0x0A;

    for (int i = 0; i < 10; i ++)
    mmu->set(address + i, test_value + i);

    mmu->set(0xFF46, address.getLocation() >> 8);

    for (int i = 0; i < 10; i ++)
    ASSERT_EQ(test_value + i, mmu->get(0xFE00 + i));
}