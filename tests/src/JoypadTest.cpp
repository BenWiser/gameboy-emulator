#include "gmock/gmock.h"

#include "Joypad.h"
#include "Mmu.h"
#include "InterruptManager.h"
#include "Bitwise.h"
#include "types/RomData.hpp"

TEST(Joypad, Direction_Keys)
{
    rom_data test_data {};

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Joypad joypad(interrupt_manager, mmu);

    Address joyp_address(0x00, 0xFF);

    // Bit 4 means direction keys enabled
    // The joypad uses false to indicate true
    byte joyp { 0 };
    bitwise::setBit(joyp, 4, false);
    bitwise::setBit(joyp, 5, true);
    mmu->set(joyp_address, joyp);

    joypad.setRight(true);

    joypad.tick();

    byte expected_value = (0x1 << 5) | 0b1110;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "Right key should unset 1st bit";

    joypad.setRight(false);
    joypad.setLeft(true);

    joypad.tick();

    expected_value = (0x1 << 5) | 0b1101;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "Left key should unset 2nd bit";

    joypad.setLeft(false);
    joypad.setUp(true);

    joypad.tick();

    expected_value = (0x1 << 5) | 0b1011;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "Up key should unset 3rd bit";

    joypad.setUp(false);
    joypad.setDown(true);

    joypad.tick();

    expected_value = (0x1 << 5) | 0b0111;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "Down key should unset 4th bit";
}

TEST(Joypad, Button_Keys)
{
    rom_data test_data {};

    auto mmu = std::make_shared<Mmu>(std::move(test_data), rom_data {});
    auto interrupt_manager = std::make_shared<InterruptManager>(mmu);
    Joypad joypad(interrupt_manager, mmu);

    Address joyp_address(0x00, 0xFF);

    // Bit 5 means button keys enabled
    // The joypad uses false to indicate true
    byte joyp { 0 };
    bitwise::setBit(joyp, 4, true);
    bitwise::setBit(joyp, 5, false);
    mmu->set(joyp_address, joyp);

    joypad.setA(true);

    joypad.tick();

    byte expected_value = (0x1 << 4) | 0b1110;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "A key should unset 1st bit";

    joypad.setA(false);
    joypad.setB(true);

    joypad.tick();

    expected_value = (0x1 << 4) | 0b1101;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "B key should unset 2nd bit";

    joypad.setB(false);
    joypad.setSelect(true);

    joypad.tick();

    expected_value = (0x1 << 4) | 0b1011;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "Select key should unset 3rd bit";

    joypad.setSelect(false);
    joypad.setStart(true);

    joypad.tick();

    expected_value = (0x1 << 4) | 0b0111;

    ASSERT_EQ(expected_value, mmu->get(joyp_address)) << "Start key should unset 4th bit";
}